﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.Net;
using System.Collections;
using System.Collections.Specialized;
using System.Text;
using System.Data;

namespace WindowsFormsApplication2
{
    /// <summary>
    /// Description of edit.
    /// </summary>
    public partial class frm_upsup : Form
    {
        ThreeTier tt = new ThreeTier();
        public string kode_sup_e, nama_sup_e, alamat_e, kota_e, propinsi_e, telp_e, no_fax_e, cp_e;
        public frm_upsup()
        {
            //
            // The InitializeComponent() call is required for Windows Forms designer support.
            //
            InitializeComponent();

            //
            // TODO: Add constructor code after the InitializeComponent() call.
            //
        }

        private void Form7_Load(object sender, EventArgs e)
        {
            kodesup.Text     = kode_sup_e;
            namasup.Text     = nama_sup_e;
            alamat.Text   = alamat_e;
            kota.Text     = kota_e;
            propinsi.Text = propinsi_e;
            telp.Text     = telp_e;
            fax.Text      = no_fax_e;
            hp.Text       = cp_e;
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            NameValueCollection data = new NameValueCollection();
            data["kode_sup"] = this.kodesup.Text;
            data["nama_sup"] = this.namasup.Text;
            data["alamat"] = this.alamat.Text;
            data["kota"] = this.kota.Text;
            data["propinsi"] = this.propinsi.Text;
            data["telp"] = this.telp.Text;
            data["no_fax"] = this.fax.Text;
            data["cp"] = this.hp.Text;
            MessageBox.Show(tt.HttpPost("http://localhost/AplikasiTA/update-data_supplier.php", data));
            this.Close();
        }        
    }
}