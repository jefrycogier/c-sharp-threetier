﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.Net;
using System.Collections;
using System.Collections.Specialized;
using System.Text;
using System.IO;
using System.Data;
using System.Reflection;
using iTextSharp.text.pdf;
using iTextSharp.text;

namespace WindowsFormsApplication2
{
    public partial class frm_laporanreturpembelian : Form
    {
        ThreeTier tt = new ThreeTier();
        public frm_laporanreturpembelian()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            NameValueCollection data = new NameValueCollection();
            string strXml = tt.HttpGet("http://localhost/AplikasiTA/tampil-data_returpembelian.php");
            System.IO.StringReader strReader = new System.IO.StringReader(strXml);
            DataSet dataSet = new DataSet();
            dataSet.ReadXml(strReader);
            try
            {
                this.dataGridView1.DataSource = dataSet.Tables[0];
                dataGridView1.ReadOnly = true;
                dataGridView1.AllowUserToAddRows = false;
            }
            catch (Exception)
            {
                MessageBox.Show("Tidak ada yang ditampilkan");
                dataGridView1.DataSource = null;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            NameValueCollection data = new NameValueCollection();
            data["no_retur"] = this.textBox1.Text;
            data["nama_supplier"] = this.textBox1.Text;
            data["tgl_trans"] = this.textBox1.Text;
            string strXml = tt.HttpPost("http://localhost/AplikasiTA/FilterPembelianRetur.php", data);
            System.IO.StringReader strReader = new System.IO.StringReader(strXml);
            DataSet dataSet = new DataSet();
            dataSet.ReadXml(strReader);
            try
            {
                dataGridView1.DataSource = dataSet.Tables[0];
            }
            catch (Exception)
            {
                MessageBox.Show("data tidak ada");
                dataGridView1.DataSource = null;
            }
        }

        private void ToPdF(DataGridView dataGridView1, string filename)
        {
            //Creating iTextSharp Table from the DataTable data
            PdfPTable pdfTable = new PdfPTable(dataGridView1.ColumnCount);
            pdfTable.DefaultCell.Padding = 3;
            pdfTable.WidthPercentage = 50;
            pdfTable.HorizontalAlignment = Element.ALIGN_CENTER;
            pdfTable.DefaultCell.BorderWidth = 1;

            //Adding Header row
            foreach (DataGridViewColumn column in dataGridView1.Columns)
            {
                PdfPCell cell = new PdfPCell(new Phrase(column.HeaderText));
                //cell.BackgroundColor = new iTextSharp.text.Color(240, 240, 240);
                pdfTable.AddCell(cell);
            }

            //Adding DataRow
            foreach (DataGridViewRow row in dataGridView1.Rows)
            {
                foreach (DataGridViewCell cell in row.Cells)
                {
                    pdfTable.AddCell(cell.Value.ToString());
                }
            }

            
                FileStream stream = new FileStream(filename, FileMode.Create);
                Document pdfDoc = new Document(PageSize.A2, 10f, 10f, 10f, 0f);
                PdfWriter.GetInstance(pdfDoc, stream);
                pdfDoc.Open();
                pdfDoc.Add(pdfTable);
                pdfDoc.Close();
                stream.Close();
                MessageBox.Show("Export data ke PDF Berhasil");
            }

        private void button3_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "PDF Documents (*.pdf)|*.pdf";
            sfd.FileName = "Laporan Retur Pembelian.pdf";
            if (sfd.ShowDialog() == DialogResult.OK)
            {
                //ToPdF(dataGridView1, @"c:\.pdf");
                ToPdF(dataGridView1, sfd.FileName); // Here dataGridview1 is your grid view name 
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            NameValueCollection data = new NameValueCollection();
            data["no_retur"] = this.textBox1.Text;
            string strXml = tt.HttpPost("http://localhost/AplikasiTA/FilterdtlPembelianRetur.php", data);
            System.IO.StringReader strReader = new System.IO.StringReader(strXml);
            DataSet dataSet = new DataSet();
            dataSet.ReadXml(strReader);
            try
            {
                dataGridView2.DataSource = dataSet.Tables[0];
            }
            catch (Exception)
            {
                MessageBox.Show("data tidak ada");
                dataGridView2.DataSource = null;
            }
        }
        }
    }

