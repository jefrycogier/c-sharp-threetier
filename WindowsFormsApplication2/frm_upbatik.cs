﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.Net;
using System.Collections;
using System.Collections.Specialized;
using System.Text;
using System.Data;

namespace WindowsFormsApplication2
{
    /// <summary>
    /// Description of edit.
    /// </summary>
    public partial class frm_upbatik : Form
    {
        ThreeTier tt = new ThreeTier();
        public string kode_bat_e, kode_pro_e, nama_bat_e, satuan_e, harga_jual_e, stok_e;
        public frm_upbatik()
        {
            //
            // The InitializeComponent() call is required for Windows Forms designer support.
            //
            InitializeComponent();

            //
            // TODO: Add constructor code after the InitializeComponent() call.
            //
        }

        void Form6Load(object sender, EventArgs e)
        {
            kodebat.Text   = kode_bat_e;
            kodepro.Text   = kode_pro_e;
            namabat.Text = nama_bat_e;
            satuan.Text = satuan_e;
            hargajual.Text = harga_jual_e;
            stok.Text      = stok_e;
        }
        
        void Button1Click(object sender, EventArgs e)
        {
            NameValueCollection data = new NameValueCollection();
            data["kode_bat"] = this.kodebat.Text;
            data["kode_pro"] = this.kodepro.Text;
            data["nama_bat"] = this.namabat.Text;
            data["satuan"] = this.satuan.Text;
            data["harga_jual"] = this.hargajual.Text;
            data["stok"] = this.stok.Text;
            MessageBox.Show(tt.HttpPost("http://localhost/AplikasiTA/update-data_batik.php", data));
            this.Close();
        }
        
    }
}
