﻿/*
 * Created by SharpDevelop.
 * User: Gede Lumbung
 * Date: 30/06/2011
 * Time: 9:26
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
namespace WindowsFormsApplication2
{
    partial class frm_upbahan
    {
        /// <summary>
        /// Designer variable used to keep track of non-visual components.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Disposes resources used by the form.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        /// <summary>
        /// This method is required for Windows Forms designer support.
        /// Do not change the method contents inside the source code editor. The Forms designer might
        /// not be able to load this method if it was changed manually.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.alamat = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.kota = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.namasup = new System.Windows.Forms.TextBox();
            this.kodesup = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(15, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 23);
            this.label1.TabIndex = 0;
            this.label1.Text = "kode :";
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(15, 32);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 23);
            this.label2.TabIndex = 2;
            this.label2.Text = "Nama :";
            // 
            // alamat
            // 
            this.alamat.Location = new System.Drawing.Point(82, 52);
            this.alamat.Multiline = true;
            this.alamat.Name = "alamat";
            this.alamat.Size = new System.Drawing.Size(52, 26);
            this.alamat.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(15, 55);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 23);
            this.label3.TabIndex = 4;
            this.label3.Text = "Jumlah bahan :";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(167, 84);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 6;
            this.button1.Text = "Update";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // kota
            // 
            this.kota.Location = new System.Drawing.Point(82, 84);
            this.kota.Multiline = true;
            this.kota.Name = "kota";
            this.kota.Size = new System.Drawing.Size(66, 26);
            this.kota.TabIndex = 8;
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(15, 87);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(64, 23);
            this.label4.TabIndex = 7;
            this.label4.Text = "Satuan :";
            // 
            // namasup
            // 
            this.namasup.Location = new System.Drawing.Point(82, 26);
            this.namasup.Name = "namasup";
            this.namasup.Size = new System.Drawing.Size(95, 20);
            this.namasup.TabIndex = 28;
            // 
            // kodesup
            // 
            this.kodesup.Location = new System.Drawing.Point(82, 3);
            this.kodesup.Name = "kodesup";
            this.kodesup.ReadOnly = true;
            this.kodesup.Size = new System.Drawing.Size(82, 20);
            this.kodesup.TabIndex = 27;
            // 
            // frm_upbahan
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(255, 121);
            this.Controls.Add(this.namasup);
            this.Controls.Add(this.kodesup);
            this.Controls.Add(this.kota);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.alamat);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "frm_upbahan";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Edit Bahan";
            this.Load += new System.EventHandler(this.Form7_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox alamat;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox kota;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox namasup;
        private System.Windows.Forms.TextBox kodesup;
    }
}