﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.Net;
using System.Collections;
using System.Collections.Specialized;
using System.Text;
using System.Data;

namespace WindowsFormsApplication2
{
    /// <summary>
    /// Description of MainForm.
    /// </summary>
    public partial class frm_supplier : Form
    {
        ThreeTier tt = new ThreeTier();
        public frm_supplier()
        {
            //
            // The InitializeComponent() call is required for Windows Forms designer support.
            //
            InitializeComponent();

            //
            // TODO: Add constructor code after the InitializeComponent() call.
            //
        }


        private void button1_Click(object sender, EventArgs e)
        {
            if (kodesup.Text == "" || namasup.Text == "" || alamat.Text == "" || kota.Text == "" || propinsi.Text == "" || telp.Text == "" || fax.Text == "" || hp.Text == "")
            {
                MessageBox.Show("Data belum lengkap");
            }
            else
            {
                NameValueCollection data = new NameValueCollection();
                data["kode_sup"] = this.kodesup.Text;
                data["nama_sup"] = this.namasup.Text;
                data["alamat"] = this.alamat.Text;
                data["kota"] = this.kota.Text;
                data["propinsi"] = this.propinsi.Text;
                data["telp"] = this.telp.Text;
                data["no_fax"] = this.fax.Text;
                data["keterangan"] = this.hp.Text;
                MessageBox.Show(tt.HttpPost("http://localhost/AplikasiTA/simpan-data_supplier.php", data));
            }

            try
            {
                this.kodesup.ReadOnly = true;
                string no_supplier = tt.HttpGet("http://localhost/AplikasiTA/GetMaxKodeSup.php");
                this.kodesup.Text = no_supplier.ToString();
            }
            catch (Exception)
            {
                MessageBox.Show("Tidak ada yang ditampilkan");
                this.Close();
            }
        }

        void DataGridView1CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            int baris = int.Parse(e.RowIndex.ToString());
            string id       = dataGridView1[0, baris].Value.ToString();
            string namasup  = dataGridView1[1, baris].Value.ToString();
            string alamat   = dataGridView1[2, baris].Value.ToString();
            string kota     = dataGridView1[3, baris].Value.ToString();
            string propinsi = dataGridView1[4, baris].Value.ToString();
            string no_telp  = dataGridView1[5, baris].Value.ToString();
            string no_fax   = dataGridView1[6, baris].Value.ToString();
            string no_cp    = dataGridView1[7, baris].Value.ToString();
            dataGridView1.ReadOnly = true;
            dataGridView1.AllowUserToAddRows = false;
            frm_upsup f1 = new frm_upsup();
            f1.kode_sup_e = id.ToString();
            f1.nama_sup_e = namasup.ToString();
            f1.alamat_e = alamat.ToString();
            f1.kota_e = kota.ToString();
            f1.propinsi_e = propinsi.ToString();
            f1.telp_e = no_telp.ToString();
            f1.no_fax_e = no_fax.ToString();
            f1.cp_e = no_cp.ToString();
            f1.ShowDialog();
        }
        void DataGridView1CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            NameValueCollection data = new NameValueCollection();
            int baris = int.Parse(e.RowIndex.ToString());
            data["kode_suphapus"] = dataGridView1[0, baris].Value.ToString();
            MessageBox.Show(tt.HttpPost("http://localhost/AplikasiTA/hapus-data_supplier.php", data));
            string strXml = tt.HttpGet("http://localhost/AplikasiTA/tampil-data_supplier.php");
            System.IO.StringReader strReader = new System.IO.StringReader(strXml);
            DataSet dataSet = new DataSet();
            dataSet.ReadXml(strReader);
            try
            {
                this.dataGridView1.DataSource = dataSet.Tables[0];
                dataGridView1.ReadOnly = true;
                dataGridView1.AllowUserToAddRows = false;
            }
            catch (Exception)
            {
                MessageBox.Show("Tidak ada yang ditampilkan");
                dataGridView1.DataSource = null;
            }
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            NameValueCollection data = new NameValueCollection();
            data["kode_sup"] = this.kodesup.Text;
            data["nama_sup"] = this.namasup.Text;
            data["alamat"] = this.alamat.Text;
            data["kota"] = this.kota.Text;
            data["propinsi"] = this.propinsi.Text;
            data["telp"] = this.telp.Text;
            data["no_fax"] = this.fax.Text;
            data["cp"] = this.hp.Text;
            string strXml = tt.HttpGet("http://localhost/AplikasiTA/tampil-data_supplier.php");
            System.IO.StringReader strReader = new System.IO.StringReader(strXml);
            DataSet dataSet = new DataSet();
            dataSet.ReadXml(strReader);
            try
            {
                this.dataGridView1.DataSource = dataSet.Tables[0];
                dataGridView1.ReadOnly = true;
                dataGridView1.AllowUserToAddRows = false;
            }
            catch (Exception)
            {
                MessageBox.Show("Tidak ada yang ditampilkan");
                dataGridView1.DataSource = null;
            }
        }

        private void frm_supplier_Load(object sender, EventArgs e)
        {
            try
            {
                this.kota.ReadOnly = true;
                this.propinsi.ReadOnly = true;
                this.kodesup.ReadOnly = true;
                string no_supplier = tt.HttpGet("http://localhost/AplikasiTA/GetMaxKodeSup.php");
                this.kodesup.Text = no_supplier.ToString();
            }
            catch (Exception)
            {
                MessageBox.Show("Tidak ada yang ditampilkan");
                this.Close();
            }

            string strXml = tt.HttpGet("http://localhost/AplikasiTA/tampil-data_kota.php");
            System.IO.StringReader strReader = new System.IO.StringReader(strXml);
            DataSet dataSet = new DataSet();
            dataSet.ReadXml(strReader);
            try
            {
                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    this.cmb_kot.Items.Add(dataSet.Tables[0].Rows[i][0].ToString());
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Tidak ada yang ditampilkan");
            }

            string strXml2 = tt.HttpGet("http://localhost/AplikasiTA/tampil-data_prov.php");
            System.IO.StringReader strReader2 = new System.IO.StringReader(strXml2);
            DataSet dataSet2 = new DataSet();
            dataSet2.ReadXml(strReader2);
            try
            {
                for (int i = 0; i < dataSet2.Tables[0].Rows.Count; i++)
                {
                    this.cmb_prov.Items.Add(dataSet2.Tables[0].Rows[i][0].ToString());
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Tidak ada yang ditampilkan");
            }
        }

        private void cmb_prov_SelectedIndexChanged(object sender, EventArgs e)
        {
            NameValueCollection data = new NameValueCollection();
            data["kode_prov"] = this.cmb_prov.SelectedItem.ToString();
            string strXml = tt.HttpGet("http://localhost/AplikasiTA/tampil-data_prov.php");
            System.IO.StringReader strReader = new System.IO.StringReader(strXml);
            DataSet dataSet = new DataSet();
            dataSet.ReadXml(strReader);

            int indek = Convert.ToInt32(this.cmb_prov.SelectedIndex.ToString());
            try
            {
                this.propinsi.Text = dataSet.Tables[0].Rows[indek][1].ToString();
            }
            catch (Exception)
            {
                MessageBox.Show("Tidak ada yang ditampilkan");
            }
        }

        private void cmb_kot_SelectedIndexChanged(object sender, EventArgs e)
        {
            NameValueCollection data = new NameValueCollection();
            data["kode_kota"] = this.cmb_kot.SelectedItem.ToString();
            string strXml = tt.HttpGet("http://localhost/AplikasiTA/tampil-data_kota.php");
            System.IO.StringReader strReader = new System.IO.StringReader(strXml);
            DataSet dataSet = new DataSet();
            dataSet.ReadXml(strReader);

            int indek = Convert.ToInt32(this.cmb_kot.SelectedIndex.ToString());
            try
            {
                this.kota.Text = dataSet.Tables[0].Rows[indek][1].ToString();
            }
            catch (Exception)
            {
                MessageBox.Show("Tidak ada yang ditampilkan");
            }
        }                  
    }
}