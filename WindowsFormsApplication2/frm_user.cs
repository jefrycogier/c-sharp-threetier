﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.Net;
using System.Collections;
using System.Collections.Specialized;
using System.Text;
using System.Data;

namespace WindowsFormsApplication2
{
    /// <summary>
    /// Description of MainForm.
    /// </summary>
    public partial class frm_user : Form
    {
        ThreeTier tt = new ThreeTier();
        public frm_user()
        {
            //
            // The InitializeComponent() call is required for Windows Forms designer support.
            //
            InitializeComponent();

            //
            // TODO: Add constructor code after the InitializeComponent() call.
            //
        }


        private void button1_Click(object sender, EventArgs e)
        {
            if (namacus.Text == "" || alamat.Text == "" || kota.Text == "" || propinsi.Text == "" || telp.Text == "" || hp.Text == "")
            {
                MessageBox.Show("Data belum lengkap");
            }
            else
            {
                NameValueCollection data = new NameValueCollection();
                data["id_user"] = this.kodecus.Text;
                data["username"] = this.telp.Text;
                data["password"] = this.hp.Text;
                data["hak_akses"] = this.propinsi.Text;
                data["nama"] = this.namacus.Text;
                data["alamat"] = this.alamat.Text;
                data["hp"] = this.kota.Text;
                MessageBox.Show(tt.HttpPost("http://localhost/AplikasiTA/simpan-data_user.php", data));
            }

            try
            {
                this.kodecus.ReadOnly = true;
                string no_user = tt.HttpGet("http://localhost/AplikasiTA/GetMaxKodeUser.php");
                this.kodecus.Text = no_user.ToString();
            }
            catch (Exception)
            {
                MessageBox.Show("Tidak ada yang ditampilkan");
                this.Close();
            }
        }

        void DataGridView1CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            int baris = int.Parse(e.RowIndex.ToString());
            string username  = dataGridView1[0, baris].Value.ToString();
            string password  = dataGridView1[1, baris].Value.ToString();
            string hak       = dataGridView1[2, baris].Value.ToString();
            string nama      = dataGridView1[3, baris].Value.ToString();
            string alamat    = dataGridView1[4, baris].Value.ToString();
            string hp        = dataGridView1[5, baris].Value.ToString();
            string id        = dataGridView1[6, baris].Value.ToString();
            dataGridView1.ReadOnly = true;
            dataGridView1.AllowUserToAddRows = false;
            frm_upuser f1 = new frm_upuser();
            f1.username_e = username.ToString();
            f1.password_e = password.ToString();
            f1.hak_akses_e = hak.ToString();
            f1.nama_e = nama.ToString();
            f1.alamat_e = alamat.ToString();
            f1.hp_e = hp.ToString();
            f1.id_user_e = id.ToString();
            f1.ShowDialog();
        }
        void DataGridView1CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            NameValueCollection data = new NameValueCollection();
            int baris = int.Parse(e.RowIndex.ToString());
            data["id_userhapus"] = dataGridView1[6, baris].Value.ToString();
            MessageBox.Show(tt.HttpPost("http://localhost/AplikasiTA/hapus-data_user.php", data));
            string strXml = tt.HttpGet("http://localhost/AplikasiTA/tampil-data_user.php");
            System.IO.StringReader strReader = new System.IO.StringReader(strXml);
            DataSet dataSet = new DataSet();
            dataSet.ReadXml(strReader);
            try
            {
                this.dataGridView1.DataSource = dataSet.Tables[0];
                dataGridView1.ReadOnly = true;
                dataGridView1.AllowUserToAddRows = false;
            }
            catch (Exception)
            {
                MessageBox.Show("Tidak ada yang ditampilkan");
                dataGridView1.DataSource = null;
            }
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            NameValueCollection data = new NameValueCollection();
            data["id_user"] = this.kodecus.Text;
            data["username"] = this.telp.Text;
            data["password"] = this.hp.Text;
            data["hak_akses"] = this.propinsi.Text;
            data["nama"] = this.namacus.Text;
            data["alamat"] = this.alamat.Text;
            data["hp"] = this.kota.Text;
            string strXml = tt.HttpGet("http://localhost/AplikasiTA/tampil-data_user.php");
            System.IO.StringReader strReader = new System.IO.StringReader(strXml);
            DataSet dataSet = new DataSet();
            dataSet.ReadXml(strReader);
            try
            {
                this.dataGridView1.DataSource = dataSet.Tables[0];
                dataGridView1.ReadOnly = true;
                dataGridView1.AllowUserToAddRows = false;
            }
            catch (Exception)
            {
                MessageBox.Show("Tidak ada yang ditampilkan");
                dataGridView1.DataSource = null;
            }
        }

        private void frm_user_Load(object sender, EventArgs e)
        {
            try
            {                
                this.kodecus.ReadOnly = true;
                string no_user = tt.HttpGet("http://localhost/AplikasiTA/GetMaxKodeUser.php");
                this.kodecus.Text = no_user.ToString();
            }
            catch (Exception)
            {
                MessageBox.Show("Tidak ada yang ditampilkan");
                this.Close();
            }
        }                  
    }
}