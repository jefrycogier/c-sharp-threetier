﻿using System;
using System.Reflection;
using System.Net;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Configuration;
using System.IO;
using iTextSharp.text.pdf;
using iTextSharp.text;

namespace WindowsFormsApplication2
{
    /// <summary>
    /// Description of MainForm.
    /// </summary>
    public partial class frm_bahan : Form
    {
        ThreeTier tt = new ThreeTier();
        public string user_e;
        public frm_bahan()
        {
            //
            // The InitializeComponent() call is required for Windows Forms designer support.
            //
            InitializeComponent();

            //
            // TODO: Add constructor code after the InitializeComponent() call.
            //
        }        

        void DataGridView1CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            int baris = int.Parse(e.RowIndex.ToString());
            string id = dataGridView1[0, baris].Value.ToString();
            string nama_bahan = dataGridView1[1, baris].Value.ToString();
            string jumlah_bahan = dataGridView1[2, baris].Value.ToString();
            string satuan = dataGridView1[3, baris].Value.ToString();
            dataGridView1.ReadOnly = true;
            dataGridView1.AllowUserToAddRows = false;
            frm_upbahan f1 = new frm_upbahan();
            f1.kode_bahan_e = id.ToString();
            f1.nama_bahan_e = nama_bahan.ToString();
            f1.jumlah_bahan_e = jumlah_bahan.ToString();
            f1.satuan_e = satuan.ToString();
            f1.ShowDialog();
        }
        void DataGridView1CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            NameValueCollection data = new NameValueCollection();
            int baris = int.Parse(e.RowIndex.ToString());
            data["kode_bahanhapus"] = dataGridView1[0, baris].Value.ToString();
            MessageBox.Show(tt.HttpPost("http://localhost/AplikasiTA/hapus-data_bahan.php", data));
            string strXml = tt.HttpGet("http://localhost/AplikasiTA/tampil-data_bahan.php");
            System.IO.StringReader strReader = new System.IO.StringReader(strXml);
            DataSet dataSet = new DataSet();
            dataSet.ReadXml(strReader);
            try
            {
                this.dataGridView1.DataSource = dataSet.Tables[0];
                dataGridView1.ReadOnly = true;
                dataGridView1.AllowUserToAddRows = false;
            }
            catch (Exception)
            {
                MessageBox.Show("Tidak ada yang ditampilkan");
                dataGridView1.DataSource = null;
            }
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            //NameValueCollection data = new NameValueCollection();
            //data["kode_bahan"] = this.kodesup.Text;
            //data["nama_bahan"] = this.namasup.Text;
            //data["jumlah_bahan"] = this.alamat.Text;
            //data["satuan"] = this.kota.Text;            
            string strXml = tt.HttpGet("http://localhost/AplikasiTA/tampil-data_bahan.php");
            System.IO.StringReader strReader = new System.IO.StringReader(strXml);
            DataSet dataSet = new DataSet();
            dataSet.ReadXml(strReader);
            try
            {
                this.dataGridView1.DataSource = dataSet.Tables[0];
                dataGridView1.ReadOnly = true;
                dataGridView1.AllowUserToAddRows = false;

                malam.Text = dataSet.Tables[0].Rows[0]["jumlah_bahan"].ToString();
                pewarna.Text = dataSet.Tables[0].Rows[1]["jumlah_bahan"].ToString();
                kain.Text = dataSet.Tables[0].Rows[2]["jumlah_bahan"].ToString(); 
            }
            catch (Exception)
            {
                MessageBox.Show("Tidak ada yang ditampilkan");
                dataGridView1.DataSource = null;
            }
        }

        private void frm_bahan_Load(object sender, EventArgs e)
        {
            try
            {
                this.kodepro.ReadOnly = true;
                string no_produksi = tt.HttpGet("http://localhost/AplikasiTA/GetMaxKodePro.php");
                this.kodepro.Text = no_produksi.ToString();
            }
            catch (Exception)
            {
                MessageBox.Show("Tidak ada yang ditampilkan");
                this.Close();
            }
            
            NameValueCollection data = new NameValueCollection();
            string strXml = tt.HttpGet("http://localhost/AplikasiTA/tampil-data_bahan.php");
            System.IO.StringReader strReader = new System.IO.StringReader(strXml);
            DataSet dataSet = new DataSet();
            dataSet.ReadXml(strReader);


            string bulan = this.dateTimePicker1.Value.Month.ToString();
            try
            {
                if (Convert.ToInt32(bulan) <= 9)
                {
                    this.txt_tanggal.Text = this.dateTimePicker1.Value.Year.ToString() + "-0" + bulan + "-" + this.dateTimePicker1.Value.Day.ToString();
                }
                else
                {
                    this.txt_tanggal.Text = this.dateTimePicker1.Value.Year.ToString() + "-" + bulan + "-" + this.dateTimePicker1.Value.Day.ToString();
                }
                malam.Text = dataSet.Tables[0].Rows[0]["jumlah_bahan"].ToString();
                pewarna.Text = dataSet.Tables[0].Rows[1]["jumlah_bahan"].ToString();
                kain.Text = dataSet.Tables[0].Rows[2]["jumlah_bahan"].ToString();
                kodebiji.Text = dataSet.Tables[0].Rows[0]["kode_bahan"].ToString();
                kodebotol.Text = dataSet.Tables[0].Rows[1]["kode_bahan"].ToString();
                kodemeter.Text = dataSet.Tables[0].Rows[2]["kode_bahan"].ToString();

            }
            catch (Exception)
            {
                MessageBox.Show("Tidak ada yang ditampilkan");                
            }

            user.Text = user_e;
      }

        private void button3_Click(object sender, EventArgs e)
        {
            int jml = Convert.ToInt32(txtjml.Text.ToString());

            int total = jml * 2;
            txtbiji.Text = total.ToString();
            int total2 = jml * 3;
            txtbotol.Text = total2.ToString();
            int total3 = jml * 1;
            txtmeter.Text = total3.ToString();

            int mlm1 = Convert.ToInt32(malam.Text.ToString());
            int mlm2 = Convert.ToInt32(txtbiji.Text.ToString());
            int total4 = mlm1 - mlm2;
            hasilbiji.Text = total4.ToString();

            int btl1 = Convert.ToInt32(pewarna.Text.ToString());
            int btl2 = Convert.ToInt32(txtbotol.Text.ToString());
            int total5 = btl1 - btl2;
            hasilbotol.Text = total5.ToString();

            int kain1 = Convert.ToInt32(kain.Text.ToString());
            int kain2 = Convert.ToInt32(txtmeter.Text.ToString());
            int total6 = kain1 - kain2;
            hasilmeter.Text = total6.ToString();

        }

        private void button4_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "Excel Documents (*.xls)|*.xls";
            sfd.FileName = "Data Bahan.xls";
            if (sfd.ShowDialog() == DialogResult.OK)
            {
                //ToCsV(dataGridView1, @"c:\export.xls");
                ToCsV(dataGridView1, sfd.FileName); // Here dataGridview1 is your grid view name 
            }
        }

        private void ToCsV(DataGridView dGV, string filename)
        {
            string stOutput = "";
            // Export titles:
            string sHeaders = "";

            for (int j = 0; j < dGV.Columns.Count; j++)
                sHeaders = sHeaders.ToString() + Convert.ToString(dGV.Columns[j].HeaderText) + "\t";
            stOutput += sHeaders + "\r\n";
            // Export data.
            for (int i = 0; i < dGV.RowCount; i++)
            {
                string stLine = "";
                for (int j = 0; j < dGV.Rows[i].Cells.Count; j++)
                    stLine = stLine.ToString() + Convert.ToString(dGV.Rows[i].Cells[j].Value) + "\t";
                stOutput += stLine + "\r\n";
            }
            Encoding utf16 = Encoding.GetEncoding(1254);
            byte[] output = utf16.GetBytes(stOutput);
            FileStream fs = new FileStream(filename, FileMode.Create);
            BinaryWriter bw = new BinaryWriter(fs);
            bw.Write(output, 0, output.Length); //write the encoded file
            bw.Flush();
            bw.Close();
            fs.Close();
        }        

        private void button9_Click(object sender, EventArgs e)
        {
            NameValueCollection data = new NameValueCollection();
            data["kode_bahan"] = this.kodebiji.Text;
            data["jumlah_bahan"] = this.hasilbiji.Text;
            MessageBox.Show(tt.HttpPost("http://localhost/AplikasiTA/update-data_stokbahan.php", data));

            NameValueCollection data2 = new NameValueCollection();
            data2["kode_bahan"] = this.kodebotol.Text;
            data2["jumlah_bahan"] = this.hasilbotol.Text;
            MessageBox.Show(tt.HttpPost("http://localhost/AplikasiTA/update-data_stokbahan.php", data2));

            NameValueCollection data3 = new NameValueCollection();
            data3["kode_bahan"] = this.kodemeter.Text;
            data3["jumlah_bahan"] = this.hasilmeter.Text;
            MessageBox.Show(tt.HttpPost("http://localhost/AplikasiTA/update-data_stokbahan.php", data3));
        }

        private void button10_Click(object sender, EventArgs e)
        {
            //NameValueCollection data = new NameValueCollection();
            //data["kode_bahan"] = this.kodebotol.Text;
            //data["jumlah_bahan"] = this.hasilbotol.Text;
            //MessageBox.Show(tt.HttpPost("http://localhost/AplikasiTA/update-data_stokbahan.php", data));
        }

        private void button11_Click(object sender, EventArgs e)
        {
            //NameValueCollection data = new NameValueCollection();
            //data["kode_bahan"] = this.kodemeter.Text;
            //data["jumlah_bahan"] = this.hasilmeter.Text;
            //MessageBox.Show(tt.HttpPost("http://localhost/AplikasiTA/update-data_stokbahan.php", data));
        }

        private void button12_Click(object sender, EventArgs e)
        {
            if (kodepro.Text == "" || txtbatik.Text == "" || txtjml.Text == "" || satuan.Text == "")
            {
                MessageBox.Show("Data belum lengkap");
            }
            else
            {
                NameValueCollection data = new NameValueCollection();
                data["kode_pro"] = this.kodepro.Text;
                data["nama_bat"] = this.txtbatik.Text;
                data["jumlah"] = this.txtjml.Text;
                data["satuan"] = this.satuan.Text;
                data["user"] = this.user.Text;
                MessageBox.Show(tt.HttpPost("http://localhost/AplikasiTA/simpan-data_produksi.php", data));

                try
                {
                    this.kodepro.ReadOnly = true;
                    string no_produksi = tt.HttpGet("http://localhost/AplikasiTA/GetMaxKodePro.php");
                    this.kodepro.Text = no_produksi.ToString();
                }
                catch (Exception)
                {
                    MessageBox.Show("Tidak ada yang ditampilkan");
                    this.Close();
                }

                txtbatik.Text = "";
                txtjml.Text = "";
                satuan.Text = "";

                
            }
               
        }

        private void button7_Click(object sender, EventArgs e)
        {
            string strXml = tt.HttpGet("http://localhost/AplikasiTA/tampil-data_produksi.php");
            System.IO.StringReader strReader = new System.IO.StringReader(strXml);
            DataSet dataSet = new DataSet();
            dataSet.ReadXml(strReader);
            try
            {
                this.dataGridView2.DataSource = dataSet.Tables[0];
                dataGridView2.ReadOnly = true;
                dataGridView2.AllowUserToAddRows = false;
            }
            catch (Exception)
            {
                MessageBox.Show("Tidak ada yang ditampilkan");
                dataGridView2.DataSource = null;
            }
        }

                         
    }
}