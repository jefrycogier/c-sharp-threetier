﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.Net;
using System.Collections;
using System.Collections.Specialized;
using System.Text;
using System.IO;
using System.Data;
using System.Reflection;
using iTextSharp.text.pdf;
using iTextSharp.text;

namespace WindowsFormsApplication2
{
    public partial class frm_pembelian : Form
    {
        ThreeTier tt = new ThreeTier();
        public string user_e;
        public frm_pembelian()
        {
            InitializeComponent();
        }



        private void button3_Click(object sender, EventArgs e)
        {
            if (notrans.Text == "" || kodebat.Text == "" || cmbnamabat.Text == "" || jumlah.Text == "" || harga.Text == "")
            {
                MessageBox.Show("Data belum lengkap");
            }
            else
            {
                if (jumlah.Text.ToString() != "")
                {
                    int bnykPesanan;
                    int bnykPsn = Convert.ToInt32(jumlah.Text.ToString());
                    int hrga = Convert.ToInt32(harga.Text.ToString());
                    int total = bnykPsn * hrga;
                    bnykPesanan = listPesanan.Rows.Count;
                    listPesanan.Rows.Insert(bnykPesanan - 1, 1);
                    listPesanan[0, bnykPesanan - 1].Value = notrans.Text;
                    listPesanan[1, bnykPesanan - 1].Value = kodebat.Text;
                    listPesanan[2, bnykPesanan - 1].Value = cmbnamabat.Text;
                    listPesanan[3, bnykPesanan - 1].Value = jumlah.Text;
                    listPesanan[4, bnykPesanan - 1].Value = harga.Text;
                    listPesanan[5, bnykPesanan - 1].Value = total;
                    //cmbnamacus.Text = "";
                    cmbnamabat.Text = "";
                    harga.Text = "";
                    jumlah.Text = "";
                }
            }

            int bnykData;
            int totalsemua = 0;
            bnykData = listPesanan.Rows.Count;
            for (int i = 0; i < bnykData - 1; i++)
            {
                totalsemua = Convert.ToInt32(totalsemua + Convert.ToInt32(listPesanan[5, i].Value));
            }
            subtotal.Text = totalsemua.ToString();
        }

        private void Form16_Load(object sender, EventArgs e)
        {
            try
            {
                this.subtotal.ReadOnly = true;
                this.notrans.ReadOnly = true;
                string no_trans = tt.HttpGet("http://localhost/AplikasiTA/GetMaxKodePem.php");
                this.notrans.Text = no_trans.ToString();
            }
            catch (Exception)
            {
                MessageBox.Show("Tidak ada yang ditampilkan");
                this.Close();
            }
            
            NameValueCollection data = new NameValueCollection();
            string strXml = tt.HttpGet("http://localhost/AplikasiTA/tampil-data_bahan.php");
            System.IO.StringReader strReader = new System.IO.StringReader(strXml);
            DataSet dataSet = new DataSet();
            dataSet.ReadXml(strReader);

            string bulan = this.dateTimePicker1.Value.Month.ToString();
            try
            {

                if (Convert.ToInt32(bulan) <= 9)
                {
                    this.txt_tanggal.Text = this.dateTimePicker1.Value.Year.ToString() + "-0" + bulan + "-" + this.dateTimePicker1.Value.Day.ToString();
                }
                else
                {
                    this.txt_tanggal.Text = this.dateTimePicker1.Value.Year.ToString() + "-" + bulan + "-" + this.dateTimePicker1.Value.Day.ToString();
                }
                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    this.cmbnamabat.Items.Add(dataSet.Tables[0].Rows[i][1].ToString());
                }
                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    this.cmbsatuan.Items.Add(dataSet.Tables[0].Rows[i][3].ToString());
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Tidak ada yang ditampilkan");
            }


            string strXml2 = tt.HttpGet("http://localhost/AplikasiTA/tampil-data_supplier.php");
            System.IO.StringReader strReader2 = new System.IO.StringReader(strXml2);
            DataSet dataSet2 = new DataSet();
            dataSet2.ReadXml(strReader2);
            try
            {
                for (int i = 0; i < dataSet2.Tables[0].Rows.Count; i++)
                {
                    this.cmbnamacus.Items.Add(dataSet2.Tables[0].Rows[i][1].ToString());
                }

            }
            catch (Exception)
            {
                MessageBox.Show("Tidak ada yang ditampilkan");
            }

            user.Text = user_e;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            int bnykData;
            int total = 0;
            bnykData = listPesanan.Rows.Count;
            for (int i = 0; i < bnykData - 1; i++)
            {
                total = Convert.ToInt32(total + Convert.ToInt32(listPesanan[5, i].Value));
            }
            subtotal.Text = total.ToString();
        }


        private void button1_Click(object sender, EventArgs e)
        {
            if (notrans.Text == "" || txt_tanggal.Text == "" || cmbnamacus.Text == "" || subtotal.Text == "")
            {
                MessageBox.Show("Data belum lengkap");
            }
            else
            {
                NameValueCollection data = new NameValueCollection();
                data["no_trans"] = this.notrans.Text;
                data["tgl_trans"] = this.txt_tanggal.Text;
                data["nama_supplier"] = this.cmbnamacus.Text;
                data["subtotal"] = this.subtotal.Text;
                MessageBox.Show(tt.HttpPost("http://localhost/AplikasiTA/simpan-data_pembelian.php", data));
            }

            try
            {
                this.notrans.ReadOnly = true;
                string no_trans = tt.HttpGet("http://localhost/AplikasiTA/GetMaxKodePem.php");
                this.notrans.Text = no_trans.ToString();
            }
            catch (Exception)
            {
                MessageBox.Show("Tidak ada yang ditampilkan");
                this.Close();
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (stok.Text == "" || jumlah.Text == "")
            {
                MessageBox.Show("Data belum terisi");
            }
            else
            {
                int stk = Convert.ToInt32(stok.Text.ToString());
                int jml = Convert.ToInt32(jumlah.Text.ToString());
                int ttl = stk + jml;
                jmlstok.Text = ttl.ToString();
            }

            if (jmlstok.Text == "")
            {
                MessageBox.Show("Data belum terisi");
            }
            else
            {
                NameValueCollection data = new NameValueCollection();
                data["kode_bahan"] = this.kodebat.Text;
                data["jumlah_bahan"] = this.jmlstok.Text;
                MessageBox.Show(tt.HttpPost("http://localhost/AplikasiTA/update-data_stokbahan.php", data));
            }
        }

        
        
        private void listPesanan_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            int baris;
            try
            {
                baris = listPesanan.CurrentCell.RowIndex;
                if (listPesanan.CurrentCell.ColumnIndex == 6)
                {
                    listPesanan.Rows.RemoveAt(baris);
                }
            }
            catch (Exception)
            {
                MessageBox.Show("durung onok isine gaes!");
            }

            subtotal.Text = "";

            int bnykData;
            int totalsemua = 0;
            bnykData = listPesanan.Rows.Count;
            for (int i = 0; i < bnykData - 1; i++)
            {
                totalsemua = Convert.ToInt32(totalsemua + Convert.ToInt32(listPesanan[5, i].Value));
            }
            subtotal.Text = totalsemua.ToString();

        }

        
        
        private void listPesanan_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            int kolom;
            try
            {
                NameValueCollection data = new NameValueCollection();
                kolom = int.Parse(e.RowIndex.ToString());
                data["no_trans"] = this.listPesanan[0, kolom].Value.ToString();
                data["kode_bahan"] = this.listPesanan[1, kolom].Value.ToString();
                data["jumlah"] = this.listPesanan[3, kolom].Value.ToString();
                data["harga"] = this.listPesanan[4, kolom].Value.ToString();
                data["total"] = this.listPesanan[5, kolom].Value.ToString();
                MessageBox.Show(tt.HttpPost("http://localhost/AplikasiTA/simpan-data_detailpembelian.php", data));
            }
            catch (Exception)
            {
                MessageBox.Show("durung onok isine bro!");
            }

        }


        private void button6_Click(object sender, EventArgs e)
        {
            //Creating iTextSharp Table from the DataTable data
            PdfPTable pdfTable = new PdfPTable(dataGridView2.ColumnCount);
            pdfTable.DefaultCell.Padding = 3;
            pdfTable.WidthPercentage = 50;
            pdfTable.HorizontalAlignment = Element.ALIGN_CENTER;
            pdfTable.DefaultCell.BorderWidth = 1;

            //Adding Header row
            foreach (DataGridViewColumn column in dataGridView2.Columns)
            {
                PdfPCell cell = new PdfPCell(new Phrase(column.HeaderText));
                //cell.BackgroundColor = new iTextSharp.text.Color(240, 240, 240);
                pdfTable.AddCell(cell);
            }

            //Adding DataRow
            foreach (DataGridViewRow row in dataGridView2.Rows)
            {
                foreach (DataGridViewCell cell in row.Cells)
                {
                    pdfTable.AddCell(cell.Value.ToString());
                }
            }

            //Exporting to PDF
            string folderPath = "D:\\DATA\\";
            if (!Directory.Exists(folderPath))
            {
                Directory.CreateDirectory(folderPath);
            }
            using (FileStream stream = new FileStream(folderPath + "Bahan.pdf", FileMode.Create))
            {
                Document pdfDoc = new Document(PageSize.A2, 10f, 10f, 10f, 0f);
                PdfWriter.GetInstance(pdfDoc, stream);
                pdfDoc.Open();
                pdfDoc.Add(pdfTable);
                pdfDoc.Close();
                stream.Close();
                MessageBox.Show("Export ke PDF Sukses");
            }
        }

        

        private void cmbnamabat_SelectedIndexChanged(object sender, EventArgs e)
        {
            NameValueCollection data = new NameValueCollection();
            data["nama_bahan"] = this.cmbnamabat.SelectedItem.ToString();
            string strXml = tt.HttpGet("http://localhost/AplikasiTA/tampil-data_bahan.php");
            System.IO.StringReader strReader = new System.IO.StringReader(strXml);
            DataSet dataSet = new DataSet();
            dataSet.ReadXml(strReader);

            int indek = Convert.ToInt32(this.cmbnamabat.SelectedIndex.ToString());
            try
            {
                this.kodebat.Text = dataSet.Tables[0].Rows[indek][0].ToString();
                this.harga.Text = dataSet.Tables[0].Rows[indek][4].ToString();
                this.stok.Text = dataSet.Tables[0].Rows[indek][2].ToString();
                this.cmbsatuan.Text = dataSet.Tables[0].Rows[indek][3].ToString();
            }
            catch (Exception)
            {
                MessageBox.Show("Tidak ada yang ditampilkan");
            }
        }

        private void cmbnamacus_SelectedIndexChanged(object sender, EventArgs e)
        {
            NameValueCollection data = new NameValueCollection();
            data["nama_sup"] = this.cmbnamacus.SelectedItem.ToString();
            string strXml = tt.HttpGet("http://localhost/AplikasiTA/tampil-data_supplier.php");
            System.IO.StringReader strReader = new System.IO.StringReader(strXml);
            DataSet dataSet = new DataSet();
            dataSet.ReadXml(strReader);

            int indek = Convert.ToInt32(this.cmbnamacus.SelectedIndex.ToString());
            try
            {
                this.kodecus.Text = dataSet.Tables[0].Rows[indek][0].ToString();

            }
            catch (Exception)
            {
                MessageBox.Show("Tidak ada yang ditampilkan");
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            string strXml = tt.HttpGet("http://localhost/AplikasiTA/tampil-data_pembelian.php");
            System.IO.StringReader strReader = new System.IO.StringReader(strXml);
            DataSet dataSet = new DataSet();
            dataSet.ReadXml(strReader);
            try
            {
                this.dataGridView2.DataSource = dataSet.Tables[0];
                dataGridView2.ReadOnly = true;
                dataGridView2.AllowUserToAddRows = false;
            }
            catch (Exception)
            {
                MessageBox.Show("Tidak ada yang ditampilkan");
                dataGridView2.DataSource = null;
            }
        }

        private void button8_Click(object sender, EventArgs e)
        {
            string strXml = tt.HttpGet("http://localhost/AplikasiTA/tampil-data_bahan.php");
            System.IO.StringReader strReader = new System.IO.StringReader(strXml);
            DataSet dataSet = new DataSet();
            dataSet.ReadXml(strReader);
            try
            {
                this.dataGridView1.DataSource = dataSet.Tables[0];
                dataGridView1.ReadOnly = true;
                dataGridView1.AllowUserToAddRows = false;
            }
            catch (Exception)
            {
                MessageBox.Show("Tidak ada yang ditampilkan");
                dataGridView1.DataSource = null;
            }
        }

    }
}
