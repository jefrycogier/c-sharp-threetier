﻿namespace WindowsFormsApplication2
{
    partial class frm_admin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_admin));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.logoutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.masterDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.inputBatikToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.laporanDataBatikToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dataCustomerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.laporanDataCustomerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dataSupplierToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.laporanSupplierToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dataUserToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.laporanUserToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.transaksiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.transaksiPembelianToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.returToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.penjualanToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.returPenjualanToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.produksiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.laporanToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.laporanPembelianToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.laporanReturPembelianToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.laporanPenjualanToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.laporanReturPenjualanToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.masterDataToolStripMenuItem,
            this.transaksiToolStripMenuItem,
            this.laporanToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(479, 24);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem,
            this.logoutToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Q)));
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(135, 22);
            this.exitToolStripMenuItem.Text = "E&xit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // logoutToolStripMenuItem
            // 
            this.logoutToolStripMenuItem.Name = "logoutToolStripMenuItem";
            this.logoutToolStripMenuItem.Size = new System.Drawing.Size(135, 22);
            this.logoutToolStripMenuItem.Text = "Logout";
            this.logoutToolStripMenuItem.Click += new System.EventHandler(this.logoutToolStripMenuItem_Click);
            // 
            // masterDataToolStripMenuItem
            // 
            this.masterDataToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.inputBatikToolStripMenuItem,
            this.dataCustomerToolStripMenuItem,
            this.dataSupplierToolStripMenuItem,
            this.dataUserToolStripMenuItem});
            this.masterDataToolStripMenuItem.Name = "masterDataToolStripMenuItem";
            this.masterDataToolStripMenuItem.Size = new System.Drawing.Size(82, 20);
            this.masterDataToolStripMenuItem.Text = "Data &Master";
            // 
            // inputBatikToolStripMenuItem
            // 
            this.inputBatikToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.laporanDataBatikToolStripMenuItem});
            this.inputBatikToolStripMenuItem.Name = "inputBatikToolStripMenuItem";
            this.inputBatikToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.inputBatikToolStripMenuItem.Text = "&Data Batik";
            this.inputBatikToolStripMenuItem.Click += new System.EventHandler(this.inputBatikToolStripMenuItem_Click);
            // 
            // laporanDataBatikToolStripMenuItem
            // 
            this.laporanDataBatikToolStripMenuItem.Name = "laporanDataBatikToolStripMenuItem";
            this.laporanDataBatikToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
            this.laporanDataBatikToolStripMenuItem.Text = "Laporan &Data Batik";
            this.laporanDataBatikToolStripMenuItem.Click += new System.EventHandler(this.laporanDataBatikToolStripMenuItem_Click);
            // 
            // dataCustomerToolStripMenuItem
            // 
            this.dataCustomerToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.laporanDataCustomerToolStripMenuItem});
            this.dataCustomerToolStripMenuItem.Name = "dataCustomerToolStripMenuItem";
            this.dataCustomerToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.dataCustomerToolStripMenuItem.Text = "&Data Customer";
            this.dataCustomerToolStripMenuItem.Click += new System.EventHandler(this.dataCustomerToolStripMenuItem_Click);
            // 
            // laporanDataCustomerToolStripMenuItem
            // 
            this.laporanDataCustomerToolStripMenuItem.Name = "laporanDataCustomerToolStripMenuItem";
            this.laporanDataCustomerToolStripMenuItem.Size = new System.Drawing.Size(199, 22);
            this.laporanDataCustomerToolStripMenuItem.Text = "Laporan &Data Customer";
            this.laporanDataCustomerToolStripMenuItem.Click += new System.EventHandler(this.laporanDataCustomerToolStripMenuItem_Click);
            // 
            // dataSupplierToolStripMenuItem
            // 
            this.dataSupplierToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.laporanSupplierToolStripMenuItem});
            this.dataSupplierToolStripMenuItem.Name = "dataSupplierToolStripMenuItem";
            this.dataSupplierToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.dataSupplierToolStripMenuItem.Text = "&Data Supplier";
            this.dataSupplierToolStripMenuItem.Click += new System.EventHandler(this.dataSupplierToolStripMenuItem_Click);
            // 
            // laporanSupplierToolStripMenuItem
            // 
            this.laporanSupplierToolStripMenuItem.Name = "laporanSupplierToolStripMenuItem";
            this.laporanSupplierToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.laporanSupplierToolStripMenuItem.Text = "Laporan &Supplier";
            this.laporanSupplierToolStripMenuItem.Click += new System.EventHandler(this.laporanSupplierToolStripMenuItem_Click);
            // 
            // dataUserToolStripMenuItem
            // 
            this.dataUserToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.laporanUserToolStripMenuItem});
            this.dataUserToolStripMenuItem.Name = "dataUserToolStripMenuItem";
            this.dataUserToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.dataUserToolStripMenuItem.Text = "Data User";
            this.dataUserToolStripMenuItem.Click += new System.EventHandler(this.dataUserToolStripMenuItem_Click);
            // 
            // laporanUserToolStripMenuItem
            // 
            this.laporanUserToolStripMenuItem.Name = "laporanUserToolStripMenuItem";
            this.laporanUserToolStripMenuItem.Size = new System.Drawing.Size(143, 22);
            this.laporanUserToolStripMenuItem.Text = "Laporan &User";
            this.laporanUserToolStripMenuItem.Click += new System.EventHandler(this.laporanUserToolStripMenuItem_Click);
            // 
            // transaksiToolStripMenuItem
            // 
            this.transaksiToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.transaksiPembelianToolStripMenuItem,
            this.returToolStripMenuItem,
            this.penjualanToolStripMenuItem,
            this.returPenjualanToolStripMenuItem,
            this.produksiToolStripMenuItem});
            this.transaksiToolStripMenuItem.Name = "transaksiToolStripMenuItem";
            this.transaksiToolStripMenuItem.Size = new System.Drawing.Size(68, 20);
            this.transaksiToolStripMenuItem.Text = "Tran&saksi";
            // 
            // transaksiPembelianToolStripMenuItem
            // 
            this.transaksiPembelianToolStripMenuItem.Name = "transaksiPembelianToolStripMenuItem";
            this.transaksiPembelianToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.transaksiPembelianToolStripMenuItem.Text = "&Pembelian";
            this.transaksiPembelianToolStripMenuItem.Click += new System.EventHandler(this.transaksiPembelianToolStripMenuItem_Click);
            // 
            // returToolStripMenuItem
            // 
            this.returToolStripMenuItem.Name = "returToolStripMenuItem";
            this.returToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.returToolStripMenuItem.Text = "&Retur Pembelian ";
            this.returToolStripMenuItem.Click += new System.EventHandler(this.returToolStripMenuItem_Click);
            // 
            // penjualanToolStripMenuItem
            // 
            this.penjualanToolStripMenuItem.Name = "penjualanToolStripMenuItem";
            this.penjualanToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.penjualanToolStripMenuItem.Text = "&Penjualan";
            this.penjualanToolStripMenuItem.Click += new System.EventHandler(this.penjualanToolStripMenuItem_Click);
            // 
            // returPenjualanToolStripMenuItem
            // 
            this.returPenjualanToolStripMenuItem.Name = "returPenjualanToolStripMenuItem";
            this.returPenjualanToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.returPenjualanToolStripMenuItem.Text = "&Retur Penjualan";
            this.returPenjualanToolStripMenuItem.Click += new System.EventHandler(this.returPenjualanToolStripMenuItem_Click);
            // 
            // produksiToolStripMenuItem
            // 
            this.produksiToolStripMenuItem.Name = "produksiToolStripMenuItem";
            this.produksiToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.produksiToolStripMenuItem.Text = "&Produksi";
            this.produksiToolStripMenuItem.Click += new System.EventHandler(this.produksiToolStripMenuItem_Click);
            // 
            // laporanToolStripMenuItem
            // 
            this.laporanToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.laporanPembelianToolStripMenuItem,
            this.laporanReturPembelianToolStripMenuItem,
            this.laporanPenjualanToolStripMenuItem,
            this.laporanReturPenjualanToolStripMenuItem});
            this.laporanToolStripMenuItem.Name = "laporanToolStripMenuItem";
            this.laporanToolStripMenuItem.Size = new System.Drawing.Size(62, 20);
            this.laporanToolStripMenuItem.Text = "Lapo&ran";
            // 
            // laporanPembelianToolStripMenuItem
            // 
            this.laporanPembelianToolStripMenuItem.Name = "laporanPembelianToolStripMenuItem";
            this.laporanPembelianToolStripMenuItem.Size = new System.Drawing.Size(207, 22);
            this.laporanPembelianToolStripMenuItem.Text = "Laporan &Pembelian";
            this.laporanPembelianToolStripMenuItem.Click += new System.EventHandler(this.laporanPembelianToolStripMenuItem_Click);
            // 
            // laporanReturPembelianToolStripMenuItem
            // 
            this.laporanReturPembelianToolStripMenuItem.Name = "laporanReturPembelianToolStripMenuItem";
            this.laporanReturPembelianToolStripMenuItem.Size = new System.Drawing.Size(207, 22);
            this.laporanReturPembelianToolStripMenuItem.Text = "Laporan &Retur Pembelian";
            this.laporanReturPembelianToolStripMenuItem.Click += new System.EventHandler(this.laporanReturPembelianToolStripMenuItem_Click);
            // 
            // laporanPenjualanToolStripMenuItem
            // 
            this.laporanPenjualanToolStripMenuItem.Name = "laporanPenjualanToolStripMenuItem";
            this.laporanPenjualanToolStripMenuItem.Size = new System.Drawing.Size(207, 22);
            this.laporanPenjualanToolStripMenuItem.Text = "Laporan &Penjualan";
            this.laporanPenjualanToolStripMenuItem.Click += new System.EventHandler(this.laporanPenjualanToolStripMenuItem_Click);
            // 
            // laporanReturPenjualanToolStripMenuItem
            // 
            this.laporanReturPenjualanToolStripMenuItem.Name = "laporanReturPenjualanToolStripMenuItem";
            this.laporanReturPenjualanToolStripMenuItem.Size = new System.Drawing.Size(207, 22);
            this.laporanReturPenjualanToolStripMenuItem.Text = "Laporan &Retur Penjualan";
            this.laporanReturPenjualanToolStripMenuItem.Click += new System.EventHandler(this.laporanReturPenjualanToolStripMenuItem_Click);
            // 
            // frm_admin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(479, 267);
            this.Controls.Add(this.menuStrip1);
            this.Name = "frm_admin";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Menu Utama Aplikasi Batik Noviaz";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem logoutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem masterDataToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem inputBatikToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem laporanDataBatikToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dataCustomerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem laporanDataCustomerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dataSupplierToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem laporanSupplierToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dataUserToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem transaksiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem transaksiPembelianToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem returToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem penjualanToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem returPenjualanToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem produksiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem laporanToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem laporanPembelianToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem laporanReturPembelianToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem laporanPenjualanToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem laporanReturPenjualanToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem laporanUserToolStripMenuItem;


    }
}