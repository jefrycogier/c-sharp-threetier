﻿/*
 * Created by SharpDevelop.
 * User: Gede Lumbung
 * Date: 30/06/2011
 * Time: 9:26
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
namespace WindowsFormsApplication2
{
    partial class Form11
    {
        /// <summary>
        /// Designer variable used to keep track of non-visual components.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Disposes resources used by the form.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        /// <summary>
        /// This method is required for Windows Forms designer support.
        /// Do not change the method contents inside the source code editor. The Forms designer might
        /// not be able to load this method if it was changed manually.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.kodecus = new System.Windows.Forms.TextBox();
            this.namacus = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.alamat = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.kota = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.telp = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.propinsi = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.hp = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(15, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 23);
            this.label1.TabIndex = 0;
            this.label1.Text = "kode :";
            // 
            // kodecus
            // 
            this.kodecus.Location = new System.Drawing.Point(82, 6);
            this.kodecus.Name = "kodecus";
            this.kodecus.ReadOnly = true;
            this.kodecus.Size = new System.Drawing.Size(82, 20);
            this.kodecus.TabIndex = 1;
            // 
            // namacus
            // 
            this.namacus.Location = new System.Drawing.Point(82, 29);
            this.namacus.Name = "namacus";
            this.namacus.Size = new System.Drawing.Size(188, 20);
            this.namacus.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(15, 32);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 23);
            this.label2.TabIndex = 2;
            this.label2.Text = "Nama :";
            // 
            // alamat
            // 
            this.alamat.Location = new System.Drawing.Point(82, 52);
            this.alamat.Multiline = true;
            this.alamat.Name = "alamat";
            this.alamat.Size = new System.Drawing.Size(361, 26);
            this.alamat.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(15, 55);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 23);
            this.label3.TabIndex = 4;
            this.label3.Text = "Alamat :";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(271, 271);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 6;
            this.button1.Text = "Update";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // kota
            // 
            this.kota.Location = new System.Drawing.Point(82, 84);
            this.kota.Multiline = true;
            this.kota.Name = "kota";
            this.kota.Size = new System.Drawing.Size(144, 26);
            this.kota.TabIndex = 8;
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(15, 87);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(64, 23);
            this.label4.TabIndex = 7;
            this.label4.Text = "Kota :";
            // 
            // telp
            // 
            this.telp.Location = new System.Drawing.Point(85, 148);
            this.telp.Multiline = true;
            this.telp.Name = "telp";
            this.telp.Size = new System.Drawing.Size(164, 26);
            this.telp.TabIndex = 10;
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(15, 151);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(64, 23);
            this.label5.TabIndex = 9;
            this.label5.Text = "Telp :";
            // 
            // propinsi
            // 
            this.propinsi.Location = new System.Drawing.Point(85, 116);
            this.propinsi.Multiline = true;
            this.propinsi.Name = "propinsi";
            this.propinsi.Size = new System.Drawing.Size(141, 26);
            this.propinsi.TabIndex = 26;
            // 
            // label10
            // 
            this.label10.Location = new System.Drawing.Point(15, 119);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(64, 23);
            this.label10.TabIndex = 25;
            this.label10.Text = "Propinsi :";
            // 
            // hp
            // 
            this.hp.Location = new System.Drawing.Point(109, 180);
            this.hp.Multiline = true;
            this.hp.Name = "hp";
            this.hp.Size = new System.Drawing.Size(161, 26);
            this.hp.TabIndex = 22;
            // 
            // label9
            // 
            this.label9.Location = new System.Drawing.Point(15, 188);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(88, 23);
            this.label9.TabIndex = 21;
            this.label9.Text = "Contact Person :";
            // 
            // Form11
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(468, 315);
            this.Controls.Add(this.propinsi);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.hp);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.telp);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.kota);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.alamat);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.namacus);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.kodecus);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "Form11";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Edit Customer";
            this.Load += new System.EventHandler(this.Form11_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox alamat;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox namacus;
        private System.Windows.Forms.TextBox kodecus;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox kota;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox telp;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox propinsi;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox hp;
        private System.Windows.Forms.Label label9;
    }
}