﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.Net;
using System.Collections;
using System.Collections.Specialized;
using System.Text;
using System.Data;

namespace WindowsFormsApplication2
{
    /// <summary>
    /// Description of edit.
    /// </summary>
    public partial class Form11 : Form
    {
        ThreeTier tt = new ThreeTier();
        public string kode_cus_e, nama_cus_e, alamat_e, kota_e, propinsi_e, telp_e, hp_e;
        public Form11()
        {
            //
            // The InitializeComponent() call is required for Windows Forms designer support.
            //
            InitializeComponent();

            //
            // TODO: Add constructor code after the InitializeComponent() call.
            //
        }

        private void Form11_Load(object sender, EventArgs e)
        {
            kodecus.Text = kode_cus_e;
            namacus.Text = nama_cus_e;
            alamat.Text   = alamat_e;
            kota.Text     = kota_e;
            propinsi.Text = propinsi_e;
            telp.Text     = telp_e;
            hp.Text = hp_e;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            NameValueCollection data = new NameValueCollection();
            data["kode_cus"] = this.kodecus.Text;
            data["nama_cus"] = this.namacus.Text;
            data["alamat"] = this.alamat.Text;
            data["kota"] = this.kota.Text;
            data["propinsi"] = this.propinsi.Text;
            data["telp"] = this.telp.Text;
            data["hp"] = this.hp.Text;
            MessageBox.Show(tt.HttpPost("http://localhost/AplikasiTA/update-data_customer.php", data));
            this.Close();
        }
   
    }
}