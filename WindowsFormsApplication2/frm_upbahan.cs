﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.Net;
using System.Collections;
using System.Collections.Specialized;
using System.Text;
using System.Data;

namespace WindowsFormsApplication2
{
    /// <summary>
    /// Description of edit.
    /// </summary>
    public partial class frm_upbahan : Form
    {
        ThreeTier tt = new ThreeTier();
        public string kode_bahan_e, nama_bahan_e, jumlah_bahan_e, satuan_e;
        public frm_upbahan()
        {
            //
            // The InitializeComponent() call is required for Windows Forms designer support.
            //
            InitializeComponent();

            //
            // TODO: Add constructor code after the InitializeComponent() call.
            //
        }

        private void Form7_Load(object sender, EventArgs e)
        {
            kodesup.Text = kode_bahan_e;
            namasup.Text = nama_bahan_e;
            alamat.Text = jumlah_bahan_e;
            kota.Text = satuan_e;        
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            NameValueCollection data = new NameValueCollection();
            data["kode_bahan"] = this.kodesup.Text;
            data["nama_bahan"] = this.namasup.Text;
            data["jumlah_bahan"] = this.alamat.Text;
            data["satuan"] = this.kota.Text;   
            MessageBox.Show(tt.HttpPost("http://localhost/AplikasiTA/update-data_bahan.php", data));
            this.Close();
        }        
    }
}