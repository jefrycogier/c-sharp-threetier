﻿namespace WindowsFormsApplication2
{
    partial class frm_supplier
    {
        /// <summary>
        /// Designer variable used to keep track of non-visual components.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Disposes resources used by the form.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        /// <summary>
        /// This method is required for Windows Forms designer support.
        /// Do not change the method contents inside the source code editor. The Forms designer might
        /// not be able to load this method if it was changed manually.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.propinsi = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.fax = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.hp = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.telp = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.kota = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.alamat = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.namasup = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.kodesup = new System.Windows.Forms.TextBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.label4 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.label5 = new System.Windows.Forms.Label();
            this.cmb_prov = new System.Windows.Forms.ComboBox();
            this.cmb_kot = new System.Windows.Forms.ComboBox();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.tabPage3.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.Padding = new System.Drawing.Point(20, 10);
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(564, 329);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.cmb_prov);
            this.tabPage1.Controls.Add(this.cmb_kot);
            this.tabPage1.Controls.Add(this.propinsi);
            this.tabPage1.Controls.Add(this.label10);
            this.tabPage1.Controls.Add(this.fax);
            this.tabPage1.Controls.Add(this.label8);
            this.tabPage1.Controls.Add(this.hp);
            this.tabPage1.Controls.Add(this.label9);
            this.tabPage1.Controls.Add(this.telp);
            this.tabPage1.Controls.Add(this.label6);
            this.tabPage1.Controls.Add(this.kota);
            this.tabPage1.Controls.Add(this.label7);
            this.tabPage1.Controls.Add(this.button1);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.alamat);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.namasup);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.kodesup);
            this.tabPage1.Location = new System.Drawing.Point(4, 36);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(556, 289);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Input Data";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // propinsi
            // 
            this.propinsi.Location = new System.Drawing.Point(191, 122);
            this.propinsi.Multiline = true;
            this.propinsi.Name = "propinsi";
            this.propinsi.Size = new System.Drawing.Size(125, 26);
            this.propinsi.TabIndex = 20;
            // 
            // label10
            // 
            this.label10.Location = new System.Drawing.Point(6, 125);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(64, 23);
            this.label10.TabIndex = 19;
            this.label10.Text = "Propinsi :";
            // 
            // fax
            // 
            this.fax.Location = new System.Drawing.Point(112, 186);
            this.fax.Multiline = true;
            this.fax.Name = "fax";
            this.fax.Size = new System.Drawing.Size(248, 26);
            this.fax.TabIndex = 18;
            // 
            // label8
            // 
            this.label8.Location = new System.Drawing.Point(6, 189);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(64, 23);
            this.label8.TabIndex = 17;
            this.label8.Text = "No. FAX :";
            // 
            // hp
            // 
            this.hp.Location = new System.Drawing.Point(112, 218);
            this.hp.Multiline = true;
            this.hp.Name = "hp";
            this.hp.Size = new System.Drawing.Size(248, 26);
            this.hp.TabIndex = 16;
            // 
            // label9
            // 
            this.label9.Location = new System.Drawing.Point(6, 221);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(88, 23);
            this.label9.TabIndex = 15;
            this.label9.Text = "Contact Person :";
            // 
            // telp
            // 
            this.telp.Location = new System.Drawing.Point(112, 155);
            this.telp.Multiline = true;
            this.telp.Name = "telp";
            this.telp.Size = new System.Drawing.Size(248, 26);
            this.telp.TabIndex = 14;
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(6, 158);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(64, 23);
            this.label6.TabIndex = 13;
            this.label6.Text = "telp :";
            // 
            // kota
            // 
            this.kota.Location = new System.Drawing.Point(190, 93);
            this.kota.Multiline = true;
            this.kota.Name = "kota";
            this.kota.Size = new System.Drawing.Size(125, 26);
            this.kota.TabIndex = 12;
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(6, 93);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(64, 23);
            this.label7.TabIndex = 11;
            this.label7.Text = "kota :";
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.Location = new System.Drawing.Point(437, 260);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(100, 23);
            this.button1.TabIndex = 6;
            this.button1.Text = "Simpan Data";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(6, 61);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 23);
            this.label3.TabIndex = 5;
            this.label3.Text = "Alamat :";
            // 
            // alamat
            // 
            this.alamat.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.alamat.Location = new System.Drawing.Point(112, 61);
            this.alamat.Multiline = true;
            this.alamat.Name = "alamat";
            this.alamat.Size = new System.Drawing.Size(425, 23);
            this.alamat.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(6, 35);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(88, 23);
            this.label2.TabIndex = 3;
            this.label2.Text = "Nama Supplier :";
            // 
            // namasup
            // 
            this.namasup.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.namasup.Location = new System.Drawing.Point(112, 35);
            this.namasup.Name = "namasup";
            this.namasup.Size = new System.Drawing.Size(425, 20);
            this.namasup.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(6, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 23);
            this.label1.TabIndex = 1;
            this.label1.Text = "Kode Supplier  :";
            // 
            // kodesup
            // 
            this.kodesup.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.kodesup.Location = new System.Drawing.Point(112, 9);
            this.kodesup.Name = "kodesup";
            this.kodesup.Size = new System.Drawing.Size(152, 20);
            this.kodesup.TabIndex = 0;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.label4);
            this.tabPage2.Controls.Add(this.button2);
            this.tabPage2.Controls.Add(this.dataGridView1);
            this.tabPage2.Location = new System.Drawing.Point(4, 36);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(556, 289);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Tampilan Data";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label4.Location = new System.Drawing.Point(6, 248);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(285, 30);
            this.label4.TabIndex = 8;
            this.label4.Text = "* Edit : Klik satu kali pada field ingin yang ingin di edit\r\n* Hapus : Klik dua k" +
                "ali pada field ingin yang ingin di hapus";
            // 
            // button2
            // 
            this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button2.Location = new System.Drawing.Point(456, 248);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(94, 23);
            this.button2.TabIndex = 1;
            this.button2.Text = "Tampilkan Data";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click_1);
            // 
            // dataGridView1
            // 
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(6, 6);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(544, 236);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridView1CellDoubleClick);
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridView1CellContentClick);
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.label5);
            this.tabPage3.Location = new System.Drawing.Point(4, 36);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(556, 289);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Info Aplikasi";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(6, 13);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(391, 51);
            this.label5.TabIndex = 2;
            this.label5.Text = "Aplikasi ini dibuat mahasiswa unesa fakultas teknik jurusan teknik informatika pr" +
                "odi manajemen informatika ================= contak person : 089661663274 WA or S" +
                "MS";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // cmb_prov
            // 
            this.cmb_prov.FormattingEnabled = true;
            this.cmb_prov.Location = new System.Drawing.Point(112, 125);
            this.cmb_prov.Name = "cmb_prov";
            this.cmb_prov.Size = new System.Drawing.Size(71, 21);
            this.cmb_prov.TabIndex = 25;
            this.cmb_prov.SelectedIndexChanged += new System.EventHandler(this.cmb_prov_SelectedIndexChanged);
            // 
            // cmb_kot
            // 
            this.cmb_kot.FormattingEnabled = true;
            this.cmb_kot.Location = new System.Drawing.Point(112, 93);
            this.cmb_kot.Name = "cmb_kot";
            this.cmb_kot.Size = new System.Drawing.Size(71, 21);
            this.cmb_kot.TabIndex = 24;
            this.cmb_kot.SelectedIndexChanged += new System.EventHandler(this.cmb_kot_SelectedIndexChanged);
            // 
            // frm_supplier
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(588, 353);
            this.Controls.Add(this.tabControl1);
            this.MaximizeBox = false;
            this.Name = "frm_supplier";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "input data supplier";
            this.Load += new System.EventHandler(this.frm_supplier_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.ResumeLayout(false);

        }
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.TextBox alamat;
        private System.Windows.Forms.TextBox namasup;
        private System.Windows.Forms.TextBox kodesup;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TextBox telp;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox kota;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox hp;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox propinsi;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox fax;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cmb_prov;
        private System.Windows.Forms.ComboBox cmb_kot;
    }
}
