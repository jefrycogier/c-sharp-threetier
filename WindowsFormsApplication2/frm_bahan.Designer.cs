﻿namespace WindowsFormsApplication2
{
    partial class frm_bahan
    {
        /// <summary>
        /// Designer variable used to keep track of non-visual components.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Disposes resources used by the form.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        /// <summary>
        /// This method is required for Windows Forms designer support.
        /// Do not change the method contents inside the source code editor. The Forms designer might
        /// not be able to load this method if it was changed manually.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.button7 = new System.Windows.Forms.Button();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.user = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.button12 = new System.Windows.Forms.Button();
            this.satuan = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.txtbatik = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.txt_tanggal = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button9 = new System.Windows.Forms.Button();
            this.kodebiji = new System.Windows.Forms.TextBox();
            this.kodebotol = new System.Windows.Forms.TextBox();
            this.kodemeter = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.hasilbiji = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.hasilbotol = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.hasilmeter = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.txtmeter = new System.Windows.Forms.TextBox();
            this.txtbotol = new System.Windows.Forms.TextBox();
            this.txtbiji = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.kain = new System.Windows.Forms.TextBox();
            this.pewarna = new System.Windows.Forms.TextBox();
            this.malam = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.txtjml = new System.Windows.Forms.TextBox();
            this.kodepro = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.label5 = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.tabPage3.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.Padding = new System.Drawing.Point(20, 10);
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(670, 455);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.button7);
            this.tabPage1.Controls.Add(this.dataGridView2);
            this.tabPage1.Location = new System.Drawing.Point(4, 36);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(662, 415);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Data Produksi";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // button7
            // 
            this.button7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button7.Location = new System.Drawing.Point(552, 333);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(94, 23);
            this.button7.TabIndex = 14;
            this.button7.Text = "Tampilkan Data";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // dataGridView2
            // 
            this.dataGridView2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Location = new System.Drawing.Point(16, 6);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.Size = new System.Drawing.Size(640, 310);
            this.dataGridView2.TabIndex = 13;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.user);
            this.tabPage2.Controls.Add(this.label1);
            this.tabPage2.Controls.Add(this.button12);
            this.tabPage2.Controls.Add(this.satuan);
            this.tabPage2.Controls.Add(this.label24);
            this.tabPage2.Controls.Add(this.txtbatik);
            this.tabPage2.Controls.Add(this.label23);
            this.tabPage2.Controls.Add(this.button4);
            this.tabPage2.Controls.Add(this.button3);
            this.tabPage2.Controls.Add(this.dateTimePicker1);
            this.tabPage2.Controls.Add(this.txt_tanggal);
            this.tabPage2.Controls.Add(this.groupBox1);
            this.tabPage2.Controls.Add(this.label11);
            this.tabPage2.Controls.Add(this.label12);
            this.tabPage2.Controls.Add(this.label13);
            this.tabPage2.Controls.Add(this.txtmeter);
            this.tabPage2.Controls.Add(this.txtbotol);
            this.tabPage2.Controls.Add(this.txtbiji);
            this.tabPage2.Controls.Add(this.label8);
            this.tabPage2.Controls.Add(this.label9);
            this.tabPage2.Controls.Add(this.label10);
            this.tabPage2.Controls.Add(this.kain);
            this.tabPage2.Controls.Add(this.pewarna);
            this.tabPage2.Controls.Add(this.malam);
            this.tabPage2.Controls.Add(this.label6);
            this.tabPage2.Controls.Add(this.label17);
            this.tabPage2.Controls.Add(this.label18);
            this.tabPage2.Controls.Add(this.txtjml);
            this.tabPage2.Controls.Add(this.kodepro);
            this.tabPage2.Controls.Add(this.label19);
            this.tabPage2.Controls.Add(this.label20);
            this.tabPage2.Controls.Add(this.label22);
            this.tabPage2.Controls.Add(this.label4);
            this.tabPage2.Controls.Add(this.button2);
            this.tabPage2.Controls.Add(this.dataGridView1);
            this.tabPage2.Location = new System.Drawing.Point(4, 36);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(662, 415);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Proses Pembuatan Batik";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // user
            // 
            this.user.Location = new System.Drawing.Point(97, 46);
            this.user.Name = "user";
            this.user.ReadOnly = true;
            this.user.Size = new System.Drawing.Size(121, 20);
            this.user.TabIndex = 108;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(58, 49);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 107;
            this.label1.Text = "User :";
            // 
            // button12
            // 
            this.button12.Location = new System.Drawing.Point(404, 134);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(89, 23);
            this.button12.TabIndex = 106;
            this.button12.Text = "Simpan Data";
            this.button12.UseVisualStyleBackColor = true;
            this.button12.Click += new System.EventHandler(this.button12_Click);
            // 
            // satuan
            // 
            this.satuan.Location = new System.Drawing.Point(142, 161);
            this.satuan.Name = "satuan";
            this.satuan.Size = new System.Drawing.Size(57, 20);
            this.satuan.TabIndex = 103;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(80, 164);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(47, 13);
            this.label24.TabIndex = 102;
            this.label24.Text = "Satuan :";
            // 
            // txtbatik
            // 
            this.txtbatik.Location = new System.Drawing.Point(142, 111);
            this.txtbatik.Name = "txtbatik";
            this.txtbatik.Size = new System.Drawing.Size(100, 20);
            this.txtbatik.TabIndex = 99;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(59, 114);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(68, 13);
            this.label23.TabIndex = 98;
            this.label23.Text = "Nama Batik :";
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(368, 379);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(88, 23);
            this.button4.TabIndex = 97;
            this.button4.Text = "Export to excel";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(258, 135);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(102, 23);
            this.button3.TabIndex = 96;
            this.button3.Text = "Proses Produksi";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(241, 31);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(44, 20);
            this.dateTimePicker1.TabIndex = 95;
            this.dateTimePicker1.Visible = false;
            // 
            // txt_tanggal
            // 
            this.txt_tanggal.Location = new System.Drawing.Point(335, 6);
            this.txt_tanggal.Name = "txt_tanggal";
            this.txt_tanggal.ReadOnly = true;
            this.txt_tanggal.Size = new System.Drawing.Size(121, 20);
            this.txt_tanggal.TabIndex = 94;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.button9);
            this.groupBox1.Controls.Add(this.kodebiji);
            this.groupBox1.Controls.Add(this.kodebotol);
            this.groupBox1.Controls.Add(this.kodemeter);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.hasilbiji);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.hasilbotol);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.hasilmeter);
            this.groupBox1.Location = new System.Drawing.Point(462, 222);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(194, 137);
            this.groupBox1.TabIndex = 93;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Sisa Bahan";
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(138, 30);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(50, 23);
            this.button9.TabIndex = 89;
            this.button9.Text = "Update";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // kodebiji
            // 
            this.kodebiji.Location = new System.Drawing.Point(4, 30);
            this.kodebiji.Name = "kodebiji";
            this.kodebiji.Size = new System.Drawing.Size(27, 20);
            this.kodebiji.TabIndex = 86;
            // 
            // kodebotol
            // 
            this.kodebotol.Location = new System.Drawing.Point(4, 57);
            this.kodebotol.Name = "kodebotol";
            this.kodebotol.Size = new System.Drawing.Size(27, 20);
            this.kodebotol.TabIndex = 87;
            // 
            // kodemeter
            // 
            this.kodemeter.Location = new System.Drawing.Point(4, 83);
            this.kodemeter.Name = "kodemeter";
            this.kodemeter.Size = new System.Drawing.Size(27, 20);
            this.kodemeter.TabIndex = 88;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(96, 86);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(33, 13);
            this.label14.TabIndex = 31;
            this.label14.Text = "meter";
            // 
            // hasilbiji
            // 
            this.hasilbiji.Location = new System.Drawing.Point(37, 30);
            this.hasilbiji.Name = "hasilbiji";
            this.hasilbiji.Size = new System.Drawing.Size(53, 20);
            this.hasilbiji.TabIndex = 26;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(96, 60);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(30, 13);
            this.label15.TabIndex = 30;
            this.label15.Text = "botol";
            // 
            // hasilbotol
            // 
            this.hasilbotol.Location = new System.Drawing.Point(37, 57);
            this.hasilbotol.Name = "hasilbotol";
            this.hasilbotol.Size = new System.Drawing.Size(53, 20);
            this.hasilbotol.TabIndex = 27;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(96, 33);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(19, 13);
            this.label16.TabIndex = 29;
            this.label16.Text = "biji";
            // 
            // hasilmeter
            // 
            this.hasilmeter.Location = new System.Drawing.Point(37, 83);
            this.hasilmeter.Name = "hasilmeter";
            this.hasilmeter.Size = new System.Drawing.Size(53, 20);
            this.hasilmeter.TabIndex = 28;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(266, 251);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(33, 13);
            this.label11.TabIndex = 92;
            this.label11.Text = "meter";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(266, 225);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(30, 13);
            this.label12.TabIndex = 91;
            this.label12.Text = "botol";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(266, 198);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(19, 13);
            this.label13.TabIndex = 90;
            this.label13.Text = "biji";
            // 
            // txtmeter
            // 
            this.txtmeter.Location = new System.Drawing.Point(207, 248);
            this.txtmeter.Name = "txtmeter";
            this.txtmeter.Size = new System.Drawing.Size(53, 20);
            this.txtmeter.TabIndex = 89;
            // 
            // txtbotol
            // 
            this.txtbotol.Location = new System.Drawing.Point(207, 222);
            this.txtbotol.Name = "txtbotol";
            this.txtbotol.Size = new System.Drawing.Size(53, 20);
            this.txtbotol.TabIndex = 88;
            // 
            // txtbiji
            // 
            this.txtbiji.Location = new System.Drawing.Point(207, 195);
            this.txtbiji.Name = "txtbiji";
            this.txtbiji.Size = new System.Drawing.Size(53, 20);
            this.txtbiji.TabIndex = 87;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(146, 251);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(33, 13);
            this.label8.TabIndex = 84;
            this.label8.Text = "meter";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(146, 225);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(30, 13);
            this.label9.TabIndex = 83;
            this.label9.Text = "botol";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(146, 198);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(19, 13);
            this.label10.TabIndex = 82;
            this.label10.Text = "biji";
            // 
            // kain
            // 
            this.kain.Location = new System.Drawing.Point(87, 248);
            this.kain.Name = "kain";
            this.kain.Size = new System.Drawing.Size(53, 20);
            this.kain.TabIndex = 81;
            // 
            // pewarna
            // 
            this.pewarna.Location = new System.Drawing.Point(87, 222);
            this.pewarna.Name = "pewarna";
            this.pewarna.Size = new System.Drawing.Size(53, 20);
            this.pewarna.TabIndex = 80;
            // 
            // malam
            // 
            this.malam.Location = new System.Drawing.Point(87, 195);
            this.malam.Name = "malam";
            this.malam.Size = new System.Drawing.Size(53, 20);
            this.malam.TabIndex = 79;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(24, 251);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(28, 13);
            this.label6.TabIndex = 78;
            this.label6.Text = "Kain";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(24, 225);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(49, 13);
            this.label17.TabIndex = 77;
            this.label17.Text = "Pewarna";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(24, 198);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(38, 13);
            this.label18.TabIndex = 76;
            this.label18.Text = "Malam";
            // 
            // txtjml
            // 
            this.txtjml.Location = new System.Drawing.Point(142, 137);
            this.txtjml.Name = "txtjml";
            this.txtjml.Size = new System.Drawing.Size(100, 20);
            this.txtjml.TabIndex = 75;
            // 
            // kodepro
            // 
            this.kodepro.Location = new System.Drawing.Point(142, 85);
            this.kodepro.Name = "kodepro";
            this.kodepro.Size = new System.Drawing.Size(100, 20);
            this.kodepro.TabIndex = 73;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(281, 9);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(52, 13);
            this.label19.TabIndex = 72;
            this.label19.Text = "Tanggal :";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(54, 140);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(73, 13);
            this.label20.TabIndex = 71;
            this.label20.Text = "Jumlah Batik :";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(45, 85);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(82, 13);
            this.label22.TabIndex = 69;
            this.label22.Text = "Kode Produksi :";
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label4.Location = new System.Drawing.Point(6, 379);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(285, 30);
            this.label4.TabIndex = 8;
            this.label4.Text = "* Edit : Klik satu kali pada field ingin yang ingin di edit\r\n* Hapus : Klik dua k" +
                "ali pada field ingin yang ingin di hapus";
            // 
            // button2
            // 
            this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button2.Location = new System.Drawing.Point(483, 374);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(140, 23);
            this.button2.TabIndex = 1;
            this.button2.Text = "Refresh dan Tampilkan";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click_1);
            // 
            // dataGridView1
            // 
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(6, 274);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(450, 102);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridView1CellDoubleClick);
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridView1CellContentClick);
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.label5);
            this.tabPage3.Location = new System.Drawing.Point(4, 36);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(662, 415);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Info Aplikasi";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(6, 13);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(337, 51);
            this.label5.TabIndex = 2;
            this.label5.Text = "setelah borang diisi semua klik Proses Produksi kemudian jika sudah fix datanya b" +
                "aru klik update kemudian simpan lalu klik refresh dan tampilkan";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // frm_bahan
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(694, 479);
            this.Controls.Add(this.tabControl1);
            this.MaximizeBox = false;
            this.Name = "frm_bahan";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Proses Produksi";
            this.Load += new System.EventHandler(this.frm_bahan_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.ResumeLayout(false);

        }
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.TextBox txt_tanggal;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox hasilbiji;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox hasilbotol;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox hasilmeter;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtmeter;
        private System.Windows.Forms.TextBox txtbotol;
        private System.Windows.Forms.TextBox txtbiji;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox kain;
        private System.Windows.Forms.TextBox pewarna;
        private System.Windows.Forms.TextBox malam;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtjml;
        private System.Windows.Forms.TextBox kodepro;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox txtbatik;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.TextBox kodebiji;
        private System.Windows.Forms.TextBox kodebotol;
        private System.Windows.Forms.TextBox kodemeter;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.TextBox satuan;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.TextBox user;
        private System.Windows.Forms.Label label1;
    }
}
