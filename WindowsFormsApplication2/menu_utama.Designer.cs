﻿namespace WindowsFormsApplication2
{
    partial class menu_utama
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(menu_utama));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.logoutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.masterDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.inputBatikToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.laporanDataBatikToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.dataCustomerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.laporanDataCustomerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dataSupplierToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.laporanSupplierToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.transaksiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.transaksiPembelianToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.returToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.penjualanToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.returPenjualanToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.produksiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.laporanToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.laporanPembelianToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.laporanReturPembelianToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.laporanPenjualanToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.laporanReturPenjualanToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.laporanDataBatikToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.masterDataToolStripMenuItem,
            this.transaksiToolStripMenuItem,
            this.laporanToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(474, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem,
            this.logoutToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Q)));
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(135, 22);
            this.exitToolStripMenuItem.Text = "E&xit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // logoutToolStripMenuItem
            // 
            this.logoutToolStripMenuItem.Name = "logoutToolStripMenuItem";
            this.logoutToolStripMenuItem.Size = new System.Drawing.Size(135, 22);
            this.logoutToolStripMenuItem.Text = "Logout";
            this.logoutToolStripMenuItem.Click += new System.EventHandler(this.logoutToolStripMenuItem_Click);
            // 
            // masterDataToolStripMenuItem
            // 
            this.masterDataToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.inputBatikToolStripMenuItem,
            this.dataCustomerToolStripMenuItem,
            this.dataSupplierToolStripMenuItem});
            this.masterDataToolStripMenuItem.Name = "masterDataToolStripMenuItem";
            this.masterDataToolStripMenuItem.Size = new System.Drawing.Size(82, 20);
            this.masterDataToolStripMenuItem.Text = "Data &Master";
            // 
            // inputBatikToolStripMenuItem
            // 
            this.inputBatikToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.laporanDataBatikToolStripMenuItem1});
            this.inputBatikToolStripMenuItem.Name = "inputBatikToolStripMenuItem";
            this.inputBatikToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.inputBatikToolStripMenuItem.Text = "Data &Batik";
            this.inputBatikToolStripMenuItem.Click += new System.EventHandler(this.inputBatikToolStripMenuItem_Click);
            // 
            // laporanDataBatikToolStripMenuItem1
            // 
            this.laporanDataBatikToolStripMenuItem1.Name = "laporanDataBatikToolStripMenuItem1";
            this.laporanDataBatikToolStripMenuItem1.Size = new System.Drawing.Size(173, 22);
            this.laporanDataBatikToolStripMenuItem1.Text = "Laporan Data Ba&tik";
            this.laporanDataBatikToolStripMenuItem1.Click += new System.EventHandler(this.laporanDataBatikToolStripMenuItem1_Click);
            // 
            // dataCustomerToolStripMenuItem
            // 
            this.dataCustomerToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.laporanDataCustomerToolStripMenuItem});
            this.dataCustomerToolStripMenuItem.Name = "dataCustomerToolStripMenuItem";
            this.dataCustomerToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.dataCustomerToolStripMenuItem.Text = "Data &Customer";
            this.dataCustomerToolStripMenuItem.Click += new System.EventHandler(this.dataCustomerToolStripMenuItem_Click);
            // 
            // laporanDataCustomerToolStripMenuItem
            // 
            this.laporanDataCustomerToolStripMenuItem.Name = "laporanDataCustomerToolStripMenuItem";
            this.laporanDataCustomerToolStripMenuItem.Size = new System.Drawing.Size(199, 22);
            this.laporanDataCustomerToolStripMenuItem.Text = "Laporan Data C&ustomer";
            this.laporanDataCustomerToolStripMenuItem.Click += new System.EventHandler(this.laporanDataCustomerToolStripMenuItem_Click);
            // 
            // dataSupplierToolStripMenuItem
            // 
            this.dataSupplierToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.laporanSupplierToolStripMenuItem});
            this.dataSupplierToolStripMenuItem.Name = "dataSupplierToolStripMenuItem";
            this.dataSupplierToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.dataSupplierToolStripMenuItem.Text = "Data &Supplier";
            this.dataSupplierToolStripMenuItem.Click += new System.EventHandler(this.dataSupplierToolStripMenuItem_Click);
            // 
            // laporanSupplierToolStripMenuItem
            // 
            this.laporanSupplierToolStripMenuItem.Name = "laporanSupplierToolStripMenuItem";
            this.laporanSupplierToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.laporanSupplierToolStripMenuItem.Text = "Laporan Data Su&pplier";
            this.laporanSupplierToolStripMenuItem.Click += new System.EventHandler(this.laporanSupplierToolStripMenuItem_Click);
            // 
            // transaksiToolStripMenuItem
            // 
            this.transaksiToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.transaksiPembelianToolStripMenuItem,
            this.returToolStripMenuItem,
            this.penjualanToolStripMenuItem,
            this.returPenjualanToolStripMenuItem,
            this.produksiToolStripMenuItem});
            this.transaksiToolStripMenuItem.Name = "transaksiToolStripMenuItem";
            this.transaksiToolStripMenuItem.Size = new System.Drawing.Size(68, 20);
            this.transaksiToolStripMenuItem.Text = "Tran&saksi";
            // 
            // transaksiPembelianToolStripMenuItem
            // 
            this.transaksiPembelianToolStripMenuItem.Name = "transaksiPembelianToolStripMenuItem";
            this.transaksiPembelianToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.transaksiPembelianToolStripMenuItem.Text = "Pe&mbelian";
            this.transaksiPembelianToolStripMenuItem.Click += new System.EventHandler(this.transaksiPembelianToolStripMenuItem_Click);
            // 
            // returToolStripMenuItem
            // 
            this.returToolStripMenuItem.Name = "returToolStripMenuItem";
            this.returToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.returToolStripMenuItem.Text = "Retur Pem&belian";
            this.returToolStripMenuItem.Click += new System.EventHandler(this.returToolStripMenuItem_Click);
            // 
            // penjualanToolStripMenuItem
            // 
            this.penjualanToolStripMenuItem.Name = "penjualanToolStripMenuItem";
            this.penjualanToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.penjualanToolStripMenuItem.Text = "&Penjualan";
            this.penjualanToolStripMenuItem.Click += new System.EventHandler(this.penjualanToolStripMenuItem_Click);
            // 
            // returPenjualanToolStripMenuItem
            // 
            this.returPenjualanToolStripMenuItem.Name = "returPenjualanToolStripMenuItem";
            this.returPenjualanToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.returPenjualanToolStripMenuItem.Text = "Retur Pen&jualan";
            this.returPenjualanToolStripMenuItem.Click += new System.EventHandler(this.returPenjualanToolStripMenuItem_Click);
            // 
            // produksiToolStripMenuItem
            // 
            this.produksiToolStripMenuItem.Name = "produksiToolStripMenuItem";
            this.produksiToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.produksiToolStripMenuItem.Text = "Pr&oduksi";
            this.produksiToolStripMenuItem.Click += new System.EventHandler(this.produksiToolStripMenuItem_Click);
            // 
            // laporanToolStripMenuItem
            // 
            this.laporanToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.laporanPembelianToolStripMenuItem,
            this.laporanReturPembelianToolStripMenuItem,
            this.laporanPenjualanToolStripMenuItem,
            this.laporanReturPenjualanToolStripMenuItem});
            this.laporanToolStripMenuItem.Name = "laporanToolStripMenuItem";
            this.laporanToolStripMenuItem.Size = new System.Drawing.Size(62, 20);
            this.laporanToolStripMenuItem.Text = "Lapo&ran";
            // 
            // laporanPembelianToolStripMenuItem
            // 
            this.laporanPembelianToolStripMenuItem.Name = "laporanPembelianToolStripMenuItem";
            this.laporanPembelianToolStripMenuItem.Size = new System.Drawing.Size(207, 22);
            this.laporanPembelianToolStripMenuItem.Text = "Laporan Pem&belian";
            this.laporanPembelianToolStripMenuItem.Click += new System.EventHandler(this.laporanPembelianToolStripMenuItem_Click);
            // 
            // laporanReturPembelianToolStripMenuItem
            // 
            this.laporanReturPembelianToolStripMenuItem.Name = "laporanReturPembelianToolStripMenuItem";
            this.laporanReturPembelianToolStripMenuItem.Size = new System.Drawing.Size(207, 22);
            this.laporanReturPembelianToolStripMenuItem.Text = "Laporan Retur Pembe&lian";
            this.laporanReturPembelianToolStripMenuItem.Click += new System.EventHandler(this.laporanReturPembelianToolStripMenuItem_Click);
            // 
            // laporanPenjualanToolStripMenuItem
            // 
            this.laporanPenjualanToolStripMenuItem.Name = "laporanPenjualanToolStripMenuItem";
            this.laporanPenjualanToolStripMenuItem.Size = new System.Drawing.Size(207, 22);
            this.laporanPenjualanToolStripMenuItem.Text = "Laporan Pen&jualan";
            this.laporanPenjualanToolStripMenuItem.Click += new System.EventHandler(this.laporanPenjualanToolStripMenuItem_Click);
            // 
            // laporanReturPenjualanToolStripMenuItem
            // 
            this.laporanReturPenjualanToolStripMenuItem.Name = "laporanReturPenjualanToolStripMenuItem";
            this.laporanReturPenjualanToolStripMenuItem.Size = new System.Drawing.Size(207, 22);
            this.laporanReturPenjualanToolStripMenuItem.Text = "Laporan &Retur Penjualan";
            this.laporanReturPenjualanToolStripMenuItem.Click += new System.EventHandler(this.laporanReturPenjualanToolStripMenuItem_Click);
            // 
            // laporanDataBatikToolStripMenuItem
            // 
            this.laporanDataBatikToolStripMenuItem.Name = "laporanDataBatikToolStripMenuItem";
            this.laporanDataBatikToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
            this.laporanDataBatikToolStripMenuItem.Text = "Laporan &Data Batik";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(405, 240);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 13);
            this.label1.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(281, 240);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(122, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Anda Login Dengan ID :";
            // 
            // menu_utama
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(474, 262);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.menuStrip1);
            this.Name = "menu_utama";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Menu Utama Aplikasi Batik Noviaz";
            this.Load += new System.EventHandler(this.menu_utama_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem masterDataToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem inputBatikToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem transaksiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem transaksiPembelianToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem laporanToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem laporanPembelianToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem logoutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dataCustomerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dataSupplierToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem returToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem penjualanToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem returPenjualanToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem produksiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem laporanDataBatikToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem laporanDataCustomerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem laporanSupplierToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem laporanReturPembelianToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem laporanPenjualanToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem laporanReturPenjualanToolStripMenuItem;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ToolStripMenuItem laporanDataBatikToolStripMenuItem1;
    }
}