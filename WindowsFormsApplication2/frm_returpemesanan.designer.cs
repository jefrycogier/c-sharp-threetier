﻿namespace WindowsFormsApplication2
{
    partial class frm_returpemesanan
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label12 = new System.Windows.Forms.Label();
            this.listPesanan = new System.Windows.Forms.DataGridView();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.label6 = new System.Windows.Forms.Label();
            this.subtotal = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.jumlah = new System.Windows.Forms.TextBox();
            this.notrans = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.harga = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.cmbsatuan = new System.Windows.Forms.ComboBox();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.txt_tanggal = new System.Windows.Forms.TextBox();
            this.stok = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.jmlstok = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.button4 = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.button6 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.cmbnamabat = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.kodebat = new System.Windows.Forms.TextBox();
            this.cmbnamacus = new System.Windows.Forms.ComboBox();
            this.kodecus = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.user = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.listPesanan)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.listPesanan);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.subtotal);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Location = new System.Drawing.Point(22, 237);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(565, 302);
            this.groupBox1.TabIndex = 51;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Tabel Pemesanan";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.ForeColor = System.Drawing.Color.Red;
            this.label12.Location = new System.Drawing.Point(11, 281);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(218, 13);
            this.label12.TabIndex = 76;
            this.label12.Text = "* klik dua kali untuk menyimpan ke database";
            // 
            // listPesanan
            // 
            this.listPesanan.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.listPesanan.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column6,
            this.Column1,
            this.Column7,
            this.Column3,
            this.Column2,
            this.Column4,
            this.Column5});
            this.listPesanan.Location = new System.Drawing.Point(6, 19);
            this.listPesanan.Name = "listPesanan";
            this.listPesanan.Size = new System.Drawing.Size(547, 224);
            this.listPesanan.TabIndex = 73;
            this.listPesanan.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.listPesanan_CellDoubleClick);
            this.listPesanan.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.listPesanan_CellContentClick);
            // 
            // Column6
            // 
            this.Column6.HeaderText = "ID Pemesanan";
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            this.Column6.Width = 50;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "ID BATIK";
            this.Column1.Name = "Column1";
            this.Column1.Width = 50;
            // 
            // Column7
            // 
            this.Column7.HeaderText = "Nama Batik";
            this.Column7.Name = "Column7";
            // 
            // Column3
            // 
            this.Column3.HeaderText = "Jumlah";
            this.Column3.Name = "Column3";
            this.Column3.Width = 70;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Harga";
            this.Column2.Name = "Column2";
            this.Column2.Width = 70;
            // 
            // Column4
            // 
            this.Column4.HeaderText = "Total";
            this.Column4.Name = "Column4";
            this.Column4.Width = 90;
            // 
            // Column5
            // 
            this.Column5.HeaderText = "";
            this.Column5.Name = "Column5";
            this.Column5.Text = "Hapus";
            this.Column5.ToolTipText = "Hapus";
            this.Column5.UseColumnTextForButtonValue = true;
            this.Column5.Width = 70;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(11, 260);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(52, 13);
            this.label6.TabIndex = 56;
            this.label6.Text = "Subtotal :";
            // 
            // subtotal
            // 
            this.subtotal.Location = new System.Drawing.Point(69, 257);
            this.subtotal.Name = "subtotal";
            this.subtotal.Size = new System.Drawing.Size(78, 20);
            this.subtotal.TabIndex = 57;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(378, 254);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(158, 23);
            this.button1.TabIndex = 43;
            this.button1.Text = "Simpan Ke Data Pemesanan";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button3
            // 
            this.button3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button3.Location = new System.Drawing.Point(524, 143);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(94, 23);
            this.button3.TabIndex = 3;
            this.button3.Text = "Tambah ke list";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // jumlah
            // 
            this.jumlah.Location = new System.Drawing.Point(248, 143);
            this.jumlah.Name = "jumlah";
            this.jumlah.Size = new System.Drawing.Size(46, 20);
            this.jumlah.TabIndex = 33;
            // 
            // notrans
            // 
            this.notrans.Location = new System.Drawing.Point(139, 87);
            this.notrans.Name = "notrans";
            this.notrans.Size = new System.Drawing.Size(100, 20);
            this.notrans.TabIndex = 31;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(19, 21);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(140, 13);
            this.label4.TabIndex = 29;
            this.label4.Text = "Tanggal Retur Pemesanan :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(174, 146);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(73, 13);
            this.label3.TabIndex = 28;
            this.label3.Text = "Jumlah Batik :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(42, 87);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 13);
            this.label1.TabIndex = 26;
            this.label1.Text = "No. Retur :";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(203, 118);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(88, 13);
            this.label17.TabIndex = 52;
            this.label17.Text = "Nama Customer :";
            // 
            // harga
            // 
            this.harga.Location = new System.Drawing.Point(412, 143);
            this.harga.Name = "harga";
            this.harga.Size = new System.Drawing.Size(72, 20);
            this.harga.TabIndex = 55;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(364, 146);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(42, 13);
            this.label5.TabIndex = 54;
            this.label5.Text = "Harga :";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(4, 146);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(68, 13);
            this.label7.TabIndex = 59;
            this.label7.Text = "Nama Batik :";
            // 
            // cmbsatuan
            // 
            this.cmbsatuan.FormattingEnabled = true;
            this.cmbsatuan.Location = new System.Drawing.Point(300, 143);
            this.cmbsatuan.Name = "cmbsatuan";
            this.cmbsatuan.Size = new System.Drawing.Size(56, 21);
            this.cmbsatuan.TabIndex = 61;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(53, 60);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(44, 20);
            this.dateTimePicker1.TabIndex = 63;
            this.dateTimePicker1.Visible = false;
            // 
            // txt_tanggal
            // 
            this.txt_tanggal.Location = new System.Drawing.Point(165, 18);
            this.txt_tanggal.Name = "txt_tanggal";
            this.txt_tanggal.ReadOnly = true;
            this.txt_tanggal.Size = new System.Drawing.Size(121, 20);
            this.txt_tanggal.TabIndex = 62;
            // 
            // stok
            // 
            this.stok.Location = new System.Drawing.Point(240, 178);
            this.stok.Name = "stok";
            this.stok.Size = new System.Drawing.Size(54, 20);
            this.stok.TabIndex = 65;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(141, 181);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(98, 13);
            this.label8.TabIndex = 64;
            this.label8.Text = "Jumlah Stok Batik :";
            // 
            // jmlstok
            // 
            this.jmlstok.Location = new System.Drawing.Point(240, 210);
            this.jmlstok.Name = "jmlstok";
            this.jmlstok.Size = new System.Drawing.Size(54, 20);
            this.jmlstok.TabIndex = 67;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(141, 213);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(85, 13);
            this.label9.TabIndex = 66;
            this.label9.Text = "Sisa Stok Batik :";
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(300, 208);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(36, 23);
            this.button4.TabIndex = 68;
            this.button4.Text = "Cek";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.button6);
            this.groupBox2.Controls.Add(this.button5);
            this.groupBox2.Controls.Add(this.dataGridView2);
            this.groupBox2.Location = new System.Drawing.Point(593, 236);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(515, 302);
            this.groupBox2.TabIndex = 69;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Tabel Stok Batik";
            // 
            // button6
            // 
            this.button6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button6.Location = new System.Drawing.Point(6, 258);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(94, 23);
            this.button6.TabIndex = 76;
            this.button6.Text = "Export";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button5
            // 
            this.button5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button5.Location = new System.Drawing.Point(406, 255);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(94, 23);
            this.button5.TabIndex = 4;
            this.button5.Text = "Refresh Data";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // dataGridView2
            // 
            this.dataGridView2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Location = new System.Drawing.Point(6, 25);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.Size = new System.Drawing.Size(494, 224);
            this.dataGridView2.TabIndex = 3;
            this.dataGridView2.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridView2CellContentClick);
            // 
            // cmbnamabat
            // 
            this.cmbnamabat.FormattingEnabled = true;
            this.cmbnamabat.Location = new System.Drawing.Point(78, 143);
            this.cmbnamabat.Name = "cmbnamabat";
            this.cmbnamabat.Size = new System.Drawing.Size(90, 21);
            this.cmbnamabat.TabIndex = 71;
            this.cmbnamabat.SelectedIndexChanged += new System.EventHandler(this.cmbkode_SelectedIndexChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(7, 178);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(65, 13);
            this.label10.TabIndex = 70;
            this.label10.Text = "Kode Batik :";
            // 
            // kodebat
            // 
            this.kodebat.Location = new System.Drawing.Point(78, 175);
            this.kodebat.Name = "kodebat";
            this.kodebat.Size = new System.Drawing.Size(46, 20);
            this.kodebat.TabIndex = 72;
            // 
            // cmbnamacus
            // 
            this.cmbnamacus.FormattingEnabled = true;
            this.cmbnamacus.Location = new System.Drawing.Point(300, 115);
            this.cmbnamacus.Name = "cmbnamacus";
            this.cmbnamacus.Size = new System.Drawing.Size(137, 21);
            this.cmbnamacus.TabIndex = 58;
            this.cmbnamacus.SelectedIndexChanged += new System.EventHandler(this.cmbnamacus_SelectedIndexChanged);
            // 
            // kodecus
            // 
            this.kodecus.Location = new System.Drawing.Point(137, 116);
            this.kodecus.Name = "kodecus";
            this.kodecus.Size = new System.Drawing.Size(46, 20);
            this.kodecus.TabIndex = 74;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(50, 116);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(85, 13);
            this.label11.TabIndex = 73;
            this.label11.Text = "Kode Customer :";
            // 
            // user
            // 
            this.user.Location = new System.Drawing.Point(347, 18);
            this.user.Name = "user";
            this.user.ReadOnly = true;
            this.user.Size = new System.Drawing.Size(121, 20);
            this.user.TabIndex = 77;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(308, 21);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(35, 13);
            this.label13.TabIndex = 76;
            this.label13.Text = "User :";
            // 
            // frm_returpemesanan
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1148, 544);
            this.Controls.Add(this.user);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.kodecus);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.kodebat);
            this.Controls.Add(this.cmbnamabat);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.jmlstok);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.stok);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.txt_tanggal);
            this.Controls.Add(this.cmbsatuan);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.cmbnamacus);
            this.Controls.Add(this.harga);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.jumlah);
            this.Controls.Add(this.notrans);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Name = "frm_returpemesanan";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Data Retur Pemesanan";
            this.Load += new System.EventHandler(this.Form16_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.listPesanan)).EndInit();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox jumlah;
        private System.Windows.Forms.TextBox notrans;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox harga;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox subtotal;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cmbsatuan;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.TextBox txt_tanggal;
        private System.Windows.Forms.TextBox stok;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox jmlstok;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.DataGridView listPesanan;
        private System.Windows.Forms.ComboBox cmbnamabat;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox kodebat;
        private System.Windows.Forms.ComboBox cmbnamacus;
        private System.Windows.Forms.TextBox kodecus;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewButtonColumn Column5;
        private System.Windows.Forms.TextBox user;
        private System.Windows.Forms.Label label13;
    }
}