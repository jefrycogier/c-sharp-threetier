﻿/*
 * Created by SharpDevelop.
 * User: Gede Lumbung
 * Date: 30/06/2011
 * Time: 9:26
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
namespace WindowsFormsApplication2
{
    partial class frm_upbatik
    {
        /// <summary>
        /// Designer variable used to keep track of non-visual components.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Disposes resources used by the form.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        /// <summary>
        /// This method is required for Windows Forms designer support.
        /// Do not change the method contents inside the source code editor. The Forms designer might
        /// not be able to load this method if it was changed manually.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.kodebat = new System.Windows.Forms.TextBox();
            this.namabat = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.hargajual = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.kodepro = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.stok = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.satuan = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 23);
            this.label1.TabIndex = 0;
            this.label1.Text = "kode batik :";
            // 
            // kodebat
            // 
            this.kodebat.Location = new System.Drawing.Point(97, 6);
            this.kodebat.Name = "kodebat";
            this.kodebat.ReadOnly = true;
            this.kodebat.Size = new System.Drawing.Size(78, 20);
            this.kodebat.TabIndex = 1;
            // 
            // namabat
            // 
            this.namabat.Location = new System.Drawing.Point(82, 63);
            this.namabat.Name = "namabat";
            this.namabat.Size = new System.Drawing.Size(138, 20);
            this.namabat.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(12, 66);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 23);
            this.label2.TabIndex = 2;
            this.label2.Text = "Nama :";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(255, 224);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 6;
            this.button1.Text = "Update";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.Button1Click);
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(12, 89);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(64, 23);
            this.label4.TabIndex = 7;
            this.label4.Text = "Satuan :";
            // 
            // hargajual
            // 
            this.hargajual.Location = new System.Drawing.Point(82, 112);
            this.hargajual.Multiline = true;
            this.hargajual.Name = "hargajual";
            this.hargajual.Size = new System.Drawing.Size(71, 26);
            this.hargajual.TabIndex = 10;
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(12, 112);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(64, 23);
            this.label5.TabIndex = 9;
            this.label5.Text = "Harga jual :";
            // 
            // kodepro
            // 
            this.kodepro.Location = new System.Drawing.Point(97, 37);
            this.kodepro.Name = "kodepro";
            this.kodepro.ReadOnly = true;
            this.kodepro.Size = new System.Drawing.Size(78, 20);
            this.kodepro.TabIndex = 12;
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(12, 40);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(111, 23);
            this.label6.TabIndex = 11;
            this.label6.Text = "kode produksi :";
            // 
            // stok
            // 
            this.stok.Location = new System.Drawing.Point(82, 144);
            this.stok.Multiline = true;
            this.stok.Name = "stok";
            this.stok.Size = new System.Drawing.Size(55, 26);
            this.stok.TabIndex = 16;
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(12, 147);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(64, 23);
            this.label7.TabIndex = 15;
            this.label7.Text = "Stok :";
            // 
            // satuan
            // 
            this.satuan.Location = new System.Drawing.Point(82, 86);
            this.satuan.Name = "satuan";
            this.satuan.Size = new System.Drawing.Size(55, 20);
            this.satuan.TabIndex = 17;
            // 
            // frm_upbatik
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(350, 281);
            this.Controls.Add(this.satuan);
            this.Controls.Add(this.stok);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.kodepro);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.hargajual);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.namabat);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.kodebat);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "frm_upbatik";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Edit Batik";
            this.Load += new System.EventHandler(this.Form6Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox namabat;
        private System.Windows.Forms.TextBox kodebat;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox hargajual;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox kodepro;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox stok;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox satuan;
    }
}