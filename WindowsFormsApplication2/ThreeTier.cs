﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.Net;
using System.Collections;
using System.Collections.Specialized;
using System.Text;
using System.Data;

namespace WindowsFormsApplication2
{
    public class ThreeTier
    {
        public ThreeTier()
        {
        }
        public String HttpPost(string uri, NameValueCollection data)
        {
            WebClient webclient = new WebClient();
            byte[] responBytes = webclient.UploadValues(uri, "POST", data);
            string result = Encoding.UTF8.GetString(responBytes);
            webclient.Dispose();
            return result;
        }

        public String HttpGet(string uri)
        {
            WebClient webclient = new WebClient();
            byte[] responBytes = webclient.DownloadData(uri);
            string result = Encoding.UTF8.GetString(responBytes);
            webclient.Dispose();
            return result;
        }
    }
}