﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.Net;
using System.Collections;
using System.Collections.Specialized;
using System.Text;
using System.Data;

namespace WindowsFormsApplication2
{
    /// <summary>
    /// Description of MainForm.
    /// </summary>
    public partial class frm_batik : Form
    {
        ThreeTier tt = new ThreeTier();
        public frm_batik()
        {
            //
            // The InitializeComponent() call is required for Windows Forms designer support.
            //
            InitializeComponent();

            //
            // TODO: Add constructor code after the InitializeComponent() call.
            //
        }


        private void button1_Click(object sender, EventArgs e)
        {
            if (satuan.Text == "" || namabat.Text == "" || cmb_pro.Text == "" || hargajual.Text == "" || stok.Text == "")
            {
                MessageBox.Show("Data belum lengkap");
            }
            else
            {
                NameValueCollection data = new NameValueCollection();
                data["kode_bat"] = this.kodebat.Text;
                data["kode_pro"] = this.cmb_pro.SelectedItem.ToString();
                data["nama_bat"] = this.namabat.Text;
                data["satuan"] = this.satuan.Text;
                data["harga_jual"] = this.hargajual.Text;
                data["stok"] = this.stok.Text;
                MessageBox.Show(tt.HttpPost("http://localhost/AplikasiTA/simpan-data_batik.php", data));
            }

            try
            {
                this.kodebat.ReadOnly = true;
                string no_batik = tt.HttpGet("http://localhost/AplikasiTA/GetMaxKodeBat.php");
                this.kodebat.Text = no_batik.ToString();
            }
            catch (Exception)
            {
                MessageBox.Show("Tidak ada yang ditampilkan");
                this.Close();
            }

            cmb_pro.Text = "";
            namabat.Text = "";
            hargajual.Text = "";
            satuan.Text = "";
            stok.Text = "";
        }

        void DataGridView1CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            int baris = int.Parse(e.RowIndex.ToString());
            string id         = dataGridView1[0, baris].Value.ToString();
            string kode_pro   = dataGridView1[1, baris].Value.ToString();
            string nama_bat   = dataGridView1[2, baris].Value.ToString();
            string satuan     = dataGridView1[3, baris].Value.ToString();
            string harga_jual = dataGridView1[4, baris].Value.ToString();
            string stok       = dataGridView1[5, baris].Value.ToString();
            dataGridView1.ReadOnly = true;
            dataGridView1.AllowUserToAddRows = false;
            frm_upbatik f1 = new frm_upbatik();
            f1.kode_bat_e = id.ToString();
            f1.kode_pro_e = kode_pro.ToString();
            f1.nama_bat_e = nama_bat.ToString();
            f1.satuan_e = satuan.ToString();
            f1.harga_jual_e = harga_jual.ToString();
            f1.stok_e = stok.ToString();
            f1.ShowDialog();
        }
        void DataGridView1CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            NameValueCollection data = new NameValueCollection();
            int baris = int.Parse(e.RowIndex.ToString());
            data["kode_bathapus"] = dataGridView1[0, baris].Value.ToString();
            MessageBox.Show(tt.HttpPost("http://localhost/AplikasiTA/hapus-data_batik.php", data));
            string strXml = tt.HttpGet("http://localhost/AplikasiTA/tampil-data_batik.php");
            System.IO.StringReader strReader = new System.IO.StringReader(strXml);
            DataSet dataSet = new DataSet();
            dataSet.ReadXml(strReader);
            try
            {
                this.dataGridView1.DataSource = dataSet.Tables[0];
                dataGridView1.ReadOnly = true;
                dataGridView1.AllowUserToAddRows = false;
            }
            catch (Exception)
            {
                MessageBox.Show("Tidak ada yang ditampilkan");
                dataGridView1.DataSource = null;
            }
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            NameValueCollection data = new NameValueCollection();
            data["kode_bat"] = this.kodebat.Text;
            data["kode_pro"] = this.satuan.Text;
            data["nama_bat"] = this.namabat.Text;
            data["satuan"] = this.cmb_pro.Text;
            data["harga_jual"] = this.hargajual.Text;
            data["stok"] = this.stok.Text;
            string strXml = tt.HttpGet("http://localhost/AplikasiTA/tampil-data_batik.php");
            System.IO.StringReader strReader = new System.IO.StringReader(strXml);
            DataSet dataSet = new DataSet();
            dataSet.ReadXml(strReader);
            try
            {
                this.dataGridView1.DataSource = dataSet.Tables[0];
                dataGridView1.ReadOnly = true;
                dataGridView1.AllowUserToAddRows = false;
            }
            catch (Exception)
            {
                MessageBox.Show("Tidak ada yang ditampilkan");
                dataGridView1.DataSource = null;
            }
        }

        

        private void Form4_Load(object sender, EventArgs e)
        {
            try
            {
                this.kodebat.ReadOnly = true;
                string no_batik = tt.HttpGet("http://localhost/AplikasiTA/GetMaxKodeBat.php");
                this.kodebat.Text = no_batik.ToString();
            }
            catch (Exception)
            {
                MessageBox.Show("Tidak ada yang ditampilkan");
                this.Close();
            }

            NameValueCollection data = new NameValueCollection();
            string strXml = tt.HttpGet("http://localhost/AplikasiTA/tampil-data_produksi.php");
            System.IO.StringReader strReader = new System.IO.StringReader(strXml);
            DataSet dataSet = new DataSet();
            dataSet.ReadXml(strReader);
            try
            {
                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    this.cmb_pro.Items.Add(dataSet.Tables[0].Rows[i][0].ToString());
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Tidak ada yang ditampilkan");
            }
        }

        private void cmb_satuan_SelectedIndexChanged(object sender, EventArgs e)
        {
            NameValueCollection data = new NameValueCollection();
            data["kode_pro"] = this.cmb_pro.SelectedItem.ToString();
            string strXml = tt.HttpGet("http://localhost/AplikasiTA/tampil-data_produksi.php");
            System.IO.StringReader strReader = new System.IO.StringReader(strXml);
            DataSet dataSet = new DataSet();
            dataSet.ReadXml(strReader);

            int indek = Convert.ToInt32(this.cmb_pro.SelectedIndex.ToString());
            try
            {
                this.namabat.Text = dataSet.Tables[0].Rows[indek][1].ToString();
                this.satuan.Text = dataSet.Tables[0].Rows[indek][3].ToString();
                this.stok.Text = dataSet.Tables[0].Rows[indek][2].ToString();
            }
            catch (Exception)
            {
                MessageBox.Show("Tidak ada yang ditampilkan");
            }
        }

       




        
                 
    }
}