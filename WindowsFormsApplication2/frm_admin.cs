﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WindowsFormsApplication2
{
    public partial class frm_admin : Form
    {
        public frm_admin()
        {
            InitializeComponent();
        }

        private void dataUserToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frm_user user = new frm_user();
            user.ShowDialog();
        }

        private void logoutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void inputBatikToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frm_batik batik = new frm_batik();
            batik.ShowDialog();
        }

        private void dataCustomerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frm_customer cuss = new frm_customer();
            cuss.ShowDialog();
        }

        private void dataSupplierToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frm_supplier sup = new frm_supplier();
            sup.ShowDialog();
        }

        private void laporanDataBatikToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frm_laporanbatik lapbat = new frm_laporanbatik();
            lapbat.ShowDialog();
        }

        private void laporanDataCustomerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frm_laporancuss lapcus = new frm_laporancuss();
            lapcus.ShowDialog();
        }

        private void laporanSupplierToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frm_laporansup lapsup = new frm_laporansup();
            lapsup.ShowDialog();
        }

        private void laporanUserToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frm_laporanuser lapuse = new frm_laporanuser();
            lapuse.ShowDialog();
        }

        private void transaksiPembelianToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frm_pembelian pembelian = new frm_pembelian();
            pembelian.ShowDialog();
        }

        private void returToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frm_returpembelian returpembelian = new frm_returpembelian();
            returpembelian.ShowDialog();
        }

        private void penjualanToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frm_pemesanan jual = new frm_pemesanan();
            jual.ShowDialog();
        }

        private void returPenjualanToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frm_returpemesanan returjual = new frm_returpemesanan();
            returjual.ShowDialog();
        }

        private void produksiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frm_bahan pro = new frm_bahan();
            pro.ShowDialog();
        }

        private void laporanPembelianToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frm_laporanpembelian laporbeli = new frm_laporanpembelian();
            laporbeli.ShowDialog();
        }

        private void laporanReturPembelianToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frm_laporanreturpembelian returlaporbeli = new frm_laporanreturpembelian();
            returlaporbeli.ShowDialog();
        }

        private void laporanPenjualanToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frm_laporanpemesanan laporjual = new frm_laporanpemesanan();
            laporjual.ShowDialog();
        }

        private void laporanReturPenjualanToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frm_laporanreturpemesanan returlaporjual = new frm_laporanreturpemesanan();
            returlaporjual.ShowDialog();
        }
    }
}
