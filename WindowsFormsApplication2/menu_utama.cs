﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.Net;
using System.Collections;
using System.Collections.Specialized;
using System.Text;
using System.Data;
using System.Drawing.Printing;
using System.IO;


namespace WindowsFormsApplication2
{
    /// <summary>
    /// Description of MainForm.
    /// </summary>
    public partial class menu_utama : Form
    {
        ThreeTier tt = new ThreeTier();
        public string user_e;
        public menu_utama()
        {
            //
            // The InitializeComponent() call is required for Windows Forms designer support.
            //
            InitializeComponent();

            //
            // TODO: Add constructor code after the InitializeComponent() call.
            //
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void logoutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }
                
        private void menu_utama_Load(object sender, EventArgs e)
        {
            label1.Text = user_e;
        }

        private void penjualanToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frm_pemesanan daftarjual = new frm_pemesanan();
            daftarjual.user_e = this.label1.Text;
            daftarjual.ShowDialog();
        }

        private void produksiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frm_bahan daftarpro = new frm_bahan();
            daftarpro.user_e = this.label1.Text;
            daftarpro.ShowDialog();
        }

        private void transaksiPembelianToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frm_pembelian daftarbeli = new frm_pembelian();
            daftarbeli.user_e = this.label1.Text;
            daftarbeli.ShowDialog();
        }

        private void laporanPenjualanToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frm_laporanpemesanan laporpesan = new frm_laporanpemesanan();
            laporpesan.ShowDialog();
        }

        private void laporanPembelianToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frm_laporanpembelian laporbeli = new frm_laporanpembelian();
            laporbeli.ShowDialog();
        }

        private void inputBatikToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frm_batik batik = new frm_batik();
            batik.ShowDialog();
        }

        private void dataCustomerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frm_customer cuss = new frm_customer();
            cuss.ShowDialog();
        }

        private void dataSupplierToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frm_supplier sup = new frm_supplier();
            sup.ShowDialog();
        }

        private void laporanDataBatikToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            frm_laporanbatik lapbat = new frm_laporanbatik();
            lapbat.ShowDialog();
        }

        private void laporanDataCustomerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frm_laporancuss lapcus = new frm_laporancuss();
            lapcus.ShowDialog();
        }

        private void laporanSupplierToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frm_laporansup lapsup = new frm_laporansup();
            lapsup.ShowDialog();
        }

        private void returToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frm_returpembelian returpembelian = new frm_returpembelian();
            returpembelian.user_e = this.label1.Text;
            returpembelian.ShowDialog();
        }

        private void returPenjualanToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frm_returpemesanan returjual = new frm_returpemesanan();
            returjual.user_e = this.label1.Text;
            returjual.ShowDialog();
        }

        private void laporanReturPembelianToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frm_laporanreturpembelian returlaporbeli = new frm_laporanreturpembelian();
            returlaporbeli.ShowDialog();
        }

        private void laporanReturPenjualanToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frm_laporanreturpemesanan returlaporjual = new frm_laporanreturpemesanan();
            returlaporjual.ShowDialog();
        }
        
     }
  }
