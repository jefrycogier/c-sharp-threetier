﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.Net;
using System.Collections;
using System.Collections.Specialized;
using System.Text;
using System.Data;

namespace WindowsFormsApplication2
{
    /// <summary>
    /// Description of MainForm.
    /// </summary>
    public partial class frm_customer : Form
    {
        ThreeTier tt = new ThreeTier();
        public frm_customer()
        {
            //
            // The InitializeComponent() call is required for Windows Forms designer support.
            //
            InitializeComponent();

            //
            // TODO: Add constructor code after the InitializeComponent() call.
            //
        }


        private void button1_Click(object sender, EventArgs e)
        {
            if (kodecus.Text == "" || namacus.Text == "" || alamat.Text == "" || kota.Text == "" || propinsi.Text == "" || telp.Text == "" || hp.Text == "")
            {
                MessageBox.Show("Data belum lengkap");
            }
            else
            {
                NameValueCollection data = new NameValueCollection();
                data["kode_cus"] = this.kodecus.Text;
                data["nama_cus"] = this.namacus.Text;
                data["alamat"] = this.alamat.Text;
                data["kota"] = this.kota.Text;
                data["propinsi"] = this.propinsi.Text;
                data["telp"] = this.telp.Text;
                data["hp"] = this.hp.Text;
                MessageBox.Show(tt.HttpPost("http://localhost/AplikasiTA/simpan-data_customer.php", data));
            }

            try
            {                
                this.kodecus.ReadOnly = true;
                string no_customer = tt.HttpGet("http://localhost/AplikasiTA/GetMaxKodeCus.php");
                this.kodecus.Text = no_customer.ToString();
            }
            catch (Exception)
            {
                MessageBox.Show("Tidak ada yang ditampilkan");
                this.Close();
            }
        }

        void DataGridView1CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            int baris = int.Parse(e.RowIndex.ToString());
            string id       = dataGridView1[0, baris].Value.ToString();
            string namacus  = dataGridView1[1, baris].Value.ToString();
            string alamat   = dataGridView1[2, baris].Value.ToString();
            string kota     = dataGridView1[3, baris].Value.ToString();
            string propinsi = dataGridView1[4, baris].Value.ToString();
            string telp  = dataGridView1[5, baris].Value.ToString();
            string hp    = dataGridView1[6, baris].Value.ToString();
            dataGridView1.ReadOnly = true;
            dataGridView1.AllowUserToAddRows = false;
            Form11 f1 = new Form11();
            f1.kode_cus_e = id.ToString();
            f1.nama_cus_e = namacus.ToString();
            f1.alamat_e = alamat.ToString();
            f1.kota_e = kota.ToString();
            f1.propinsi_e = propinsi.ToString();
            f1.telp_e = telp.ToString();
            f1.hp_e = hp.ToString();
            f1.ShowDialog();
        }
        void DataGridView1CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            NameValueCollection data = new NameValueCollection();
            int baris = int.Parse(e.RowIndex.ToString());
            data["kode_cushapus"] = dataGridView1[0, baris].Value.ToString();
            MessageBox.Show(tt.HttpPost("http://localhost/AplikasiTA/hapus-data_customer.php", data));
            string strXml = tt.HttpGet("http://localhost/AplikasiTA/tampil-data_customer.php");
            System.IO.StringReader strReader = new System.IO.StringReader(strXml);
            DataSet dataSet = new DataSet();
            dataSet.ReadXml(strReader);
            try
            {
                this.dataGridView1.DataSource = dataSet.Tables[0];
                dataGridView1.ReadOnly = true;
                dataGridView1.AllowUserToAddRows = false;
            }
            catch (Exception)
            {
                MessageBox.Show("Tidak ada yang ditampilkan");
                dataGridView1.DataSource = null;
            }
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            NameValueCollection data = new NameValueCollection();
            data["kode_cus"] = this.kodecus.Text;
            data["nama_cus"] = this.namacus.Text;
            data["alamat"] = this.alamat.Text;
            data["kota"] = this.kota.Text;
            data["propinsi"] = this.propinsi.Text;
            data["no_telp"] = this.telp.Text;
            data["no_cp"] = this.hp.Text;
            string strXml = tt.HttpGet("http://localhost/AplikasiTA/tampil-data_customer.php");
            System.IO.StringReader strReader = new System.IO.StringReader(strXml);
            DataSet dataSet = new DataSet();
            dataSet.ReadXml(strReader);
            try
            {
                this.dataGridView1.DataSource = dataSet.Tables[0];
                dataGridView1.ReadOnly = true;
                dataGridView1.AllowUserToAddRows = false;
            }
            catch (Exception)
            {
                MessageBox.Show("Tidak ada yang ditampilkan");
                dataGridView1.DataSource = null;
            }
        }

        private void frm_customer_Load(object sender, EventArgs e)
        {
            try
            {
                this.kota.ReadOnly = true;
                this.propinsi.ReadOnly = true;
                this.kodecus.ReadOnly = true;
                string no_customer = tt.HttpGet("http://localhost/AplikasiTA/GetMaxKodeCus.php");
                this.kodecus.Text = no_customer.ToString();
            }
            catch (Exception)
            {
                MessageBox.Show("Tidak ada yang ditampilkan");
                this.Close();
            }

            string strXml = tt.HttpGet("http://localhost/AplikasiTA/tampil-data_kota.php");
            System.IO.StringReader strReader = new System.IO.StringReader(strXml);
            DataSet dataSet = new DataSet();
            dataSet.ReadXml(strReader);
            try
            {
                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    this.cmb_kot.Items.Add(dataSet.Tables[0].Rows[i][0].ToString());
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Tidak ada yang ditampilkan");
            }

            string strXml2 = tt.HttpGet("http://localhost/AplikasiTA/tampil-data_prov.php");
            System.IO.StringReader strReader2 = new System.IO.StringReader(strXml2);
            DataSet dataSet2 = new DataSet();
            dataSet2.ReadXml(strReader2);
            try
            {
                for (int i = 0; i < dataSet2.Tables[0].Rows.Count; i++)
                {
                    this.cmb_prov.Items.Add(dataSet2.Tables[0].Rows[i][0].ToString());
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Tidak ada yang ditampilkan");
            }
        }

        private void cmb_kot_SelectedIndexChanged(object sender, EventArgs e)
        {
            NameValueCollection data = new NameValueCollection();
            data["kode_kota"] = this.cmb_kot.SelectedItem.ToString();
            string strXml = tt.HttpGet("http://localhost/AplikasiTA/tampil-data_kota.php");
            System.IO.StringReader strReader = new System.IO.StringReader(strXml);
            DataSet dataSet = new DataSet();
            dataSet.ReadXml(strReader);

            int indek = Convert.ToInt32(this.cmb_kot.SelectedIndex.ToString());
            try
            {
                this.kota.Text = dataSet.Tables[0].Rows[indek][1].ToString();                
            }
            catch (Exception)
            {
                MessageBox.Show("Tidak ada yang ditampilkan");
            }
        }

        private void cmb_prov_SelectedIndexChanged(object sender, EventArgs e)
        {
            NameValueCollection data = new NameValueCollection();
            data["kode_prov"] = this.cmb_prov.SelectedItem.ToString();
            string strXml = tt.HttpGet("http://localhost/AplikasiTA/tampil-data_prov.php");
            System.IO.StringReader strReader = new System.IO.StringReader(strXml);
            DataSet dataSet = new DataSet();
            dataSet.ReadXml(strReader);

            int indek = Convert.ToInt32(this.cmb_prov.SelectedIndex.ToString());
            try
            {
                this.propinsi.Text = dataSet.Tables[0].Rows[indek][1].ToString();
            }
            catch (Exception)
            {
                MessageBox.Show("Tidak ada yang ditampilkan");
            }
        }                  
    }
}