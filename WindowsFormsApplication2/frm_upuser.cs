﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.Net;
using System.Collections;
using System.Collections.Specialized;
using System.Text;
using System.Data;

namespace WindowsFormsApplication2
{
    /// <summary>
    /// Description of edit.
    /// </summary>
    public partial class frm_upuser : Form
    {
        ThreeTier tt = new ThreeTier();
        public string username_e, password_e, hak_akses_e, nama_e, alamat_e, hp_e, id_user_e;
        public frm_upuser()
        {
            //
            // The InitializeComponent() call is required for Windows Forms designer support.
            //
            InitializeComponent();

            //
            // TODO: Add constructor code after the InitializeComponent() call.
            //
        }

        private void frm_upuser_Load(object sender, EventArgs e)
        {
            kodecus.Text = id_user_e;
            namacus.Text = nama_e;
            alamat.Text   = alamat_e;
            kota.Text     = hp_e;
            propinsi.Text = hak_akses_e;
            hp.Text = username_e;
            keterangan.Text = password_e;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            NameValueCollection data = new NameValueCollection();
            data["id_user"] = this.kodecus.Text;
            data["username"] = this.hp.Text;
            data["password"] = this.keterangan.Text;
            data["hak_akses"] = this.propinsi.Text;
            data["nama"] = this.namacus.Text;
            data["alamat"] = this.alamat.Text;
            data["hp"] = this.kota.Text;            
            MessageBox.Show(tt.HttpPost("http://localhost/AplikasiTA/update-data_user.php", data));
            this.Close();
        }
   
    }
}