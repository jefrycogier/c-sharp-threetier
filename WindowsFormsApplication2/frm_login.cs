﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.Net;
using System.Collections;
using System.Collections.Specialized;
using System.Text;
using System.Data;

namespace WindowsFormsApplication2
{
    /// <summary>
    /// Description of MainForm.
    /// </summary>
    public partial class frm_login : Form
    {
        ThreeTier tt = new ThreeTier();
        public frm_login()
        {
            //
            // The InitializeComponent() call is required for Windows Forms designer support.
            //
            InitializeComponent();

            //
            // TODO: Add constructor code after the InitializeComponent() call.
            //
        }

        void Btn_simpanClick(object sender, EventArgs e)
        {
            this.login();
        }
        public string id_user;
        protected void login()
        {
            if (txt_password.Text != "" && txt_username.Text != "")
            {
                NameValueCollection data = new NameValueCollection();
                data["username"] = this.txt_username.Text;
                data["password"] = this.txt_password.Text;
                string hak = tt.HttpPost("http://localhost/AplikasiTA/login.php", data);
                if (hak == "user")
                {
                    menu_utama menu_user = new menu_utama();
                    id_user = tt.HttpPost("http://localhost/AplikasiTA/session.php", data);
                    menu_user.user_e = this.txt_username.Text;
                    menu_user.Show();
                }
                else if (hak == "admin")
                {
                    frm_admin menu_admin = new frm_admin();
                    menu_admin.ShowDialog();
                }
                else
                {
                    MessageBox.Show("Anda Tidak Mempunyai Akses Masuk !", "Kesalahan", MessageBoxButtons.OK);
                }
            }
            else
            {
                MessageBox.Show("Lengkapi kolom username dan password secara benar", "Kesalahan", MessageBoxButtons.OK);
            }

            txt_username.Text = "";
            txt_password.Text = "";
        }

        void Txt_passwordKeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                this.login();

                e.Handled = true;
            }
            else if (e.KeyChar == (char)Keys.Back)
            {
                this.txt_password.Text = "";
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
        }

        


    }
}
